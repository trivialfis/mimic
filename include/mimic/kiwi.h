/* Copyright (c) 2018 Jiaming Yuan <jm.yuan@outlook.com>
 *
 * This file is part of Kiwi.
 *
 * Kiwi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kiwi is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kiwi.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KIWI_H_
#define KIWI_H_

#ifdef __cplusplus
#define __Kiwi_EXTERN_C extern "C"
#else
#define __Kiwi_EXTERN_C
#endif

#if defined(_MSC_VER) || defined(_WIN32) || defined (__CYGWIN__)
#error "Windows is not supported."
#elif defined(__GNUC__)

#define KiwiAPI      __Kiwi_EXTERN_C __attribute__ ((visibility ("default")))
#define KiwiInternal __Kiwi_EXTERN_C __attribute__ ((visibility ("hidden")))

#else

#define KiwiAPI      __Kiwi_EXTERN_C
#define KiwiInternal __Kiwi_EXTERN_C

#endif

/*!
 * \brief Opaque type representing parameter.
 */
typedef struct __Kiwi_parameter* Kiwi_parameter;

/*!
 * \brief Opaque type representing model.
 */
typedef struct __Kiwi_model* Kiwi_model;
typedef struct __Kiwi_sequence* Kiwi_sequence;

enum __Kiwi_ErrorCode {
  kSuccess = 0,
  kWarning = 1,
  kFatal = 2
};
typedef int Kiwi_ErrorCode;

/*!
 * \brief Set global parameter.
 */
KiwiAPI Kiwi_ErrorCode
Kiwi_globalSetParam(char const* key, char const* value);

/*!
 * \brief Returen a model.
 */
KiwiAPI Kiwi_model
Kiwi_createModel(char const* model_str);

/*!
 *\ brief Destroy a model
 */
KiwiAPI Kiwi_ErrorCode
Kiwi_destroyModel(Kiwi_model model);

/*!
 *  \brief Initialize model.
 */
KiwiAPI int
Kiwi_initModel(Kiwi_model _model);

/*!
 * \brief Set model parameter;
 */
KiwiAPI int
Kiwi_modelSetParam(Kiwi_model _model, Kiwi_parameter _param);

/*!
 * \brief Prediction
 */
KiwiAPI int
Kiwi_predict(Kiwi_model _model, float* result);

/*!
 * \breif Prediction with uncertainty.
 */
KiwiAPI int
Kiwi_predictUncertainty(Kiwi_model _model, float* uncertainty);

KiwiAPI Kiwi_sequence Kiwi_loadFile(char* dirpath);
KiwiAPI Kiwi_ErrorCode Kiwi_destroySequence(Kiwi_sequence seq);

#endif  // KIWI_H_