#ifndef _BASIC_FILESYSTEM_HH_
#define _BASIC_FILESYSTEM_HH_

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>

#include <string>
#include <vector>

#include "mimic/basic/strings.hh"

namespace mimic {

struct Directory {
 public:
  DIR *dir_;

  explicit Directory(std::string dirname);
  ~Directory() { closedir(dir_); }
};

class TemporaryDirectory {
 public:
  /*!
   * \brief Default constructor.
   *        Creates a new temporary directory with a unique name.
   * \param verbose whether to emit extra messages
   */
  explicit TemporaryDirectory(bool verbose = false);

  std::string const& GetPath() const {
    return path_;
  }

  /*! \brief Destructor. Will perform recursive deletion via RecursiveDelete() */
  ~TemporaryDirectory() {
    RemoveDirectoryRecursively(path_.c_str());
  }

 private:
  /*! \brief Whether to emit extra messages */
  bool verbose_;
  /*! \brief Full path of the temporary directory */
  std::string path_;

  /*!
   * \brief Determine whether a given path is a symbolic link
   * \param path String representation of path
   */
  bool IsSymlink(const std::string& path) {
    struct stat sb;
    return S_ISLNK(sb.st_mode);
  }

  void RemoveDirectoryRecursively(std::string const& dirname);
};

class Path {
  u8string path_;

 public:
  static Path join(Path const& lhs, Path const& rhs) {
    Path res = lhs;
    if (endswith(lhs.path_, "/") && startswith(rhs.path_, "/")) {
      res.path_ = res.path_.substr(0, res.path_.length() - 1);
    }
    res.path_ += rhs.path_;
    return res;
  }

  Path() = default;
  Path(Path const& other) : path_ { other.path_ }{}
  Path(Path&& other) : path_(std::move(other.path_)) {}
  Path(u8string const& path) : path_{path} {}

  Path& operator=(Path const& other) {
    path_ = other.path_;
    return *this;
  }
  Path& operator=(Path&& other) {
    path_ = std::move(other.path_);
    return *this;
  }
  Path& operator=(u8string other) {
    path_ = other;
    return *this;
  }

  Path join(Path other) const {
    return join(*this, std::move(other));
  }

  bool exists() const {
    int is_exist = access(path_.c_str(), F_OK);
    return is_exist == 0 ? true : false;
  }

  u8string const& str() const {
    return path_;
  }

  char const* raw() const {
    return path_.c_str();
  }

  friend std::ostream& operator<<(std::ostream& os, Path const& path) {
    os << path.str().cpp_str();
    return os;
  }
};

}  // namespace mimic

#endif  // _BASIC_FILESYSTEM_HH_