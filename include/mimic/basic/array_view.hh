#ifndef BASIC_ARRAY_VIEW_HH_
#define BASIC_ARRAY_VIEW_HH_

#include <mimic/basic.h>
#include <iterator>

namespace mimic {

template <typename T>
class ArrayView {
 private:
  template <typename ViewType>
  class ArrayViewIterator {
   public:
    using iterator_category = std::random_access_iterator_tag; // NOLINT
    using difference_type = typename ViewType::index_type;
    using value_type =
        typename std::remove_cv<typename ViewType::element_type>::type;
    using index_type = typename ViewType::index_type;

   public:
    ArrayViewIterator(ArrayView const* const view, index_type index) :
        view_{view}, index_{index} {}
    ~ArrayViewIterator() = default;

    ArrayViewIterator(ArrayViewIterator const& other) :
        index_{other.index_},
        view_{other.view_} {}
    ArrayViewIterator(ArrayViewIterator&& other) :
        index_{other.index_},
        view_{other.view_} {}

    T operator*() const {
      return view_[index_];
    }

   private:
    McInt index_;
    ViewType const* view_;
  };

 public:
  using index_type = McInt;                     // NOLINT
  using iterator = ArrayViewIterator<ArrayView<T>>; // NOLINT

 public:
  ArrayView(T* ptr, McInt size): data_{ptr}, size_{size} {}

  T operator[](index_type ind) const {
    return data_[ind];
  }

  index_type size() const { return size_; }
  index_type sizeBytes() const { return size_ * sizeof(T); }

  iterator begin() const {
    return iterator(this, 0);
  }
  iterator const end() const {
    return iterator(this, size_);
  }
  iterator const cbegin() const {
    return begin();
  }
  iterator const cend() const {
    return end();
  }

 private:
  McInt const size_;
  T const* const data_;
};

}  // namespace mimic


#endif  // BASIC_ARRAY_VIEW_HH_