#ifndef BASIC_RANDOM_HH_
#define BASIC_RANDOM_HH_

#include <random>

namespace mimic {

/*! \brief Wrapper of std::random_device for seeding. */
class RandomDevice {
  using ResultType = std::random_device::result_type;

 private:
  static int seed_;
  static bool is_seeded_;

 public:
  static int seed(int s);
  static int reset();

  static ResultType gen();
};

/*! \brief Set random seed during the life time of context object. */
class RandomContext {
 private:
  bool should_reset_;

 public:
  RandomContext() : should_reset_{false} {}
  RandomContext(int seed) : should_reset_{true} {
    RandomDevice::seed(seed);
  }
  RandomContext(RandomContext const& other) = delete;
  RandomContext(RandomContext&& other) :
      should_reset_ { other.shouldReset() } {
    other.should_reset_ = false;
  }

  RandomContext& operator=(RandomContext const& other) = delete;
  RandomContext& operator=(RandomContext&& other) {
    should_reset_ = other.shouldReset();
    other.should_reset_ = false;
    return *this;
  }

  bool shouldReset() const { return should_reset_; }

  ~RandomContext() {
    if (should_reset_) {
      RandomDevice::reset();
    }
  }
};

}  // namespace mimic

#endif  // BASIC_RANDOM_HH_