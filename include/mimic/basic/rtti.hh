//===- llvm/Support/Casting.h - Allow flexible, checked, casts --*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT included in llvm repository for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the isa<X>(), cast<X>()
//
//===----------------------------------------------------------------------===//

// Derived from LLVM Casting.h.

#ifndef BASIC_RTTI_HH_
#define BASIC_RTTI_HH_

#include <type_traits>
#include <cassert>
#include <memory>

namespace mimic {

/// If T is a pointer to X, return a pointer to const X. If it is not,
/// return const T.
template <typename T, typename Enable = void>
struct AddConstPastPointer {
  using Type = const T;
};

template <typename T>
struct AddConstPastPointer<
  T, typename std::enable_if<std::is_pointer<T>::value>::type> {
  using Type = const typename std::remove_pointer<T>::type*;
};

template <typename T, typename Enabled = void>
struct AddLValueReferenceIfNotPointer {
  using Type = T&;
};

template <typename T>
struct AddLValueReferenceIfNotPointer<T,
                                      typename std::enable_if<
                                        std::is_pointer<T>::value>::type> {
  using Type = T;
};

template<typename From>
struct SimplifyType {
  using SimpleType = From;
  static SimpleType getSimplifiedValue(From& val) {
    return val;
  }
};

template <typename From>
struct SimplifyType<const From> {
  using NonConstSimpleType = typename SimplifyType<From>::SimpleType;
  using SimpleType =
      typename AddConstPastPointer<NonConstSimpleType>::Type;
  using RetType =
      typename AddLValueReferenceIfNotPointer<SimpleType>::Type;

  static RetType getSimplifiedValue(From const& val) {
    return SimplifyType<From>::getSimplifiedValue(const_cast<From&>(val));
  }
};

template <typename To, typename From, typename Enabler = void>
struct IsaImpl {
  static bool exe(const From& object) {
    return To::isClassOf(&object);
  }
};

// Enable upcast.
template <typename To, typename From>
struct IsaImpl<
  To, From, typename std::enable_if<std::is_base_of<To, From>::value>::type> {
  static bool exe(const From&) { return true; }
};

// Const value
template <typename To, typename From>
struct IsaImplCl {
  static bool exe(const From& val) {
    return IsaImpl<To, From>::exe(val);
  }
};

template <typename To, typename From>
struct IsaImplCl<To, const From> {
  static bool exe(const From& val) {
    return IsaImpl<To, From>::exe(val);
  }
};

// Const pointer
template <typename To, typename From>
struct IsaImplCl<To, From*> {
  static bool exe(const From* val) {
    assert(val && "isa<> used on a null pointer");
    return IsaImplCl<To, From>::exe(*val);
  }
};

// Accept unique_ptr
template <typename To, typename From>
struct IsaImplCl<To, const std::unique_ptr<From>> {
  static bool exe(const std::unique_ptr<From> &val) {
    assert(val && "isa<> used on a null pointer");
    return IsaImplCl<To, From>::exe(*val);
  }
};

// Remove const pointer
template <typename To, typename From>
struct IsaImplCl<To, From* const> {
  static bool exe(From* const val) {
    assert(val && "isa<> used on a null pointer");
    return IsaImpl<To, From>::exe(*val);
  }
};

// const pointer to const value
template <typename To, typename From>
struct IsaImplCl<To, From const* const> {
  static bool exe(From const *val) {
    assert(val && "isa<> used on a null pointer");
    return IsaImpl<To, From>::exe(*val);
  }
};

template <typename To, typename From, typename SimpleFrom>
struct IsaImplWrap {
  static bool exe(From const& val) {
    return IsaImplWrap<To, SimpleFrom,
                       typename SimplifyType<SimpleFrom>::SimpleType>::exe(
                           SimplifyType<const From>::getSimplifiedValue(val));
  }
};

template<typename To, typename FromTy>
struct IsaImplWrap<To, FromTy, FromTy> {
  static bool exe(FromTy const& val) {
    return IsaImplCl<To, FromTy>::exe(val);
  }
};

template <typename X, typename From>
bool isa(const From& val) {
  return IsaImplWrap<X, From const,
                     typename SimplifyType<From const>::SimpleType>::exe(val);
}

//===----------------------------------------------------------------------===//
//                          cast<x> Support Templates
//===----------------------------------------------------------------------===//

template <typename To, typename From>
struct CastRetty;

// Calculate what type the 'cast' function should return, based on a requested
// type of To and a source type of From.
template <typename To, typename From>
struct CastRettyImpl {
  using RetType = To&;  // Normal case, return Ty&
};

template <typename To, typename From>
struct CastRettyImpl<To, From const> {
  using RetType = To const&;  // Normal case, return Ty&
};

template <typename To, typename From>
struct CastRettyImpl<To, From*> {
  using RetType = To *;
};

template <typename To, typename From>
struct CastRettyImpl<To, From const*> {
  using RetType = To const*;
};

template <typename To, typename From>
struct CastRettyImpl<To, From const* const> {
  using RetType = To const*;
};

template <typename To, typename From>
struct CastRettyImpl<To, std::unique_ptr<From>> {
 private:
  using PointerType = typename CastRettyImpl<To, From*>::RetType;
  using ResultType = typename std::remove_pointer<PointerType>::type;

 public:
  using RetType = std::unique_ptr<ResultType>;
};

template <typename To, typename From, typename SimpleForm>
struct CastRettyWrap {
  // When the simplified type and the from type are not the same, use the type
  // simplifier to reduce the type, then reuse cast_retty_impl to get the
  // resultant type.
  using RetType = typename CastRetty<To, SimpleForm>::RetType;
};

template <typename To, typename FromTy>
struct CastRettyWrap<To, FromTy, FromTy> {
  // When the simplified type is equal to the from type, use it directly.
  using RetType = typename CastRettyImpl<To, FromTy>::RetType;
};

template <typename To, typename From>
struct CastRetty {
  using RetType =
      typename CastRettyWrap<To, From,
                             typename SimplifyType<From>::SimpleType>::RetType;
};

// Ensure the non-simple values are converted using the simplify_type template
// that may be specialized by smart pointers...
//
template <typename To, typename From, typename SimpleFrom>
struct CastConvertVal {
  // Recursive call
  // This is not a simple type, use the template to simplify it...
  static typename CastRetty<To, From>::RetType exe(From &val) {
    return CastConvertVal<To, SimpleFrom,
                          typename SimplifyType<From>::SimpleType>::exe(
                              SimplifyType<From>::getSimplifiedValue(val));
  }
};

template <typename To, typename FromTy>
struct CastConvertVal<To, FromTy, FromTy> {
  // This _is_ a simple type, just cast it.
  static typename CastRetty<To, FromTy>::RetType exe(FromTy const& val) {
    typename CastRetty<To, FromTy>::RetType res2
        = (typename CastRetty<To, FromTy>::RetType)const_cast<FromTy&>(val);
    return res2;
  }
};

template <typename X>
struct IsSimpleType {
  static bool const value =
      std::is_same<X, typename SimplifyType<X>::SimpleType>::value;
};

// cast<X> - Return the argument parameter cast to the specified type.  This
// casting operator asserts that the type is correct, so it does not return null
// on failure.  It does not allow a null argument (use cast_or_null for that).
// It is typically used like this:
//
//  cast<Instruction>(myVal)->getParent()
//

// Not a simple type
template <typename X, typename Y>
typename std::enable_if<!IsSimpleType<Y>::value,
                        typename CastRetty<X, Y const>::RetType>::type
cast(Y const& val) {
  assert(isa<X>(val) && "cast<Ty>() argument of incompatible type!");
  return CastConvertVal<X, Y const,
                        typename SimplifyType<Y const>::SimpleType>::exe(val);
}

// A simple type
template <typename X, typename Y>
typename CastRetty<X, Y>::RetType cast(Y& val) {
  assert(isa<X>(val) && "cast<Ty>() argument of incompatible type!");
  return CastConvertVal<X, Y, typename SimplifyType<Y>::SimpleType>::exe(val);
}

template <typename X, typename Y>
typename CastRetty<X, Y*>::RetType cast(Y* val) {
  assert(isa<X>(val) && "cast<Ty>() argument of incompatible type!");
  return CastConvertVal<X, Y*, typename SimplifyType<Y*>::SimpleType>::exe(val);
}

template <typename X, typename Y>
typename CastRetty<X, std::unique_ptr<Y>>::RetType
cast(std::unique_ptr<Y>&& val) {
  assert(isa<X>(val.get()) && "cast<Ty>() argument of incompatible type!");
  using RetType = typename CastRetty<X, std::unique_ptr<Y>>::RetType;
  return RetType(
      CastConvertVal<X, Y*, typename SimplifyType<Y*>::SimpleType>::exe(
          val.release()));
}

}  // namespace mimic

#endif  // BASIC_RTTI_HH_