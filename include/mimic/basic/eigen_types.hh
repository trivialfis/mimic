#ifndef BASIC_EIGEN_TYPES_HH_
#define BASIC_EIGEN_TYPES_HH_

#include <algorithm>

#include <Eigen/Dense>
#include <mimic/basic.h>

#include <nih/logging.hh>
#include "range.hh"

namespace mimic {

using MatrixXfC =
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>;
using MatrixXfR =
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

inline Eigen::VectorXf amax(MatrixXfC const& value, McInt axis) {
  if (axis == 0) {
    Eigen::VectorXf res (value.cols());
    for (auto c : Range(value.rows())) {
      auto const& col = value.col(c);
      McFloat const* maximum =
          std::max_element(col.data(), col.data() + col.size());
      res(c) = *maximum;
    }
    return res;
  } else if (axis == 1) {
    Eigen::VectorXf res (value.rows());
    for (auto r : Range(value.rows())) {
      // Column major requires explicity copy.
      Eigen::VectorXf const& row = value.row(r);
      McFloat const* maximum =
          std::max_element(row.data(), row.data() + row.size());
      res(r) = *maximum;
    }
    return res;
  } else {
    LOG(FATAL) << "Invalid axis: " << axis;
    return Eigen::VectorXf(0);
  }
}

}  // namespace mimic

#endif  // BASIC_EIGEN_TYPES_HH_