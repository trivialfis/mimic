#ifndef BASIC_CACHE_HH_
#define BASIC_CACHE_HH_

#include <utility>

namespace mimic {

template <typename Ty>
struct SingleThreadedPolicy {
  SingleThreadedPolicy() = default;
  ~SingleThreadedPolicy() = default;

  using VolatileType = Ty;

  template <typename OtherT>
  struct CalcVolatileType {
    using Type = OtherT;
  };
};

template <typename Type, typename ThreadPolicy = SingleThreadedPolicy<Type>>
struct Cache {
 private:
  typename ThreadPolicy::VolatileType value_;
  typename ThreadPolicy::template CalcVolatileType<bool>::Type latest_;

 public:
  explicit Cache() : latest_{false} {}
  Cache(Type const& value) : value_{value}, latest_{true} {}
  virtual ~Cache() = default;

  void obsolate() {
    if (latest_ != false) {
      ThreadPolicy guard;
      latest_ = false;
    }
  }
  void update(Type&& value) {
    ThreadPolicy guard;
    value_ = std::move(value);
    latest_ = true;
  }
  void update(Type const& value) {
    ThreadPolicy guard;
    value_ = value;
    latest_ = true;
  }

  bool isnew() const { return latest_; }
  Type get() const { return value_; }
};

}  // namespace mimic

#endif  // BASIC_CACHE_HH_