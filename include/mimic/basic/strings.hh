#ifndef _BASIC_STRINGS_HH_
#define _BASIC_STRINGS_HH_

#include <vector>
#include <string>

#include "mimic/basic.h"
#include "tinyutf8.h"

namespace mimic {

using u8string = utf8_string;

template <typename Functor>
void Split(std::vector<std::string>& result, std::string const& input,
           Functor func) {
  size_t last_pos = 0;
  size_t len = 0;
  for (auto c : input) {
    if (!func(c)) {
      len++;
    } else {
      result.push_back(input.substr(last_pos, len));
      last_pos += len + 1;
      len = 0;
    }
  }
  if (input.size() - last_pos > 0) {
    result.push_back(input.substr(last_pos));
  }
}

template<typename T>
T ston(std::string const& s);

template <>
inline float ston<float>(std::string const& s) {
  return std::stod(s);
}

template <>
inline double ston<double>(std::string const& s) {
  return std::stod(s);
}

template <>
inline int ston<int>(std::string const& s) {
  return std::stoi(s);
}

template <>
inline McInt ston<McInt>(std::string const& s) {
  return std::stol(s);
}

inline bool endswith(u8string const& str, u8string const& postfix) {
  u8string const& substr = str.substr(str.length() - postfix.length());
  return substr == postfix;
}

inline bool startswith(u8string const& str, u8string const& prefix) {
  u8string const& substr = str.substr(0, prefix.length());
  return substr == prefix;
}

}  // namespace mimic

#endif  // _BASIC_STRINGS_HH_