#ifndef BASIC_TIMER_HH_
#define BASIC_TIMER_HH_

#include <chrono>
#include <string>

namespace mimic {
class Timer {
  using Clock = std::chrono::high_resolution_clock;
  using TimePoint = Clock::time_point;
  using Duration = Clock::duration;
  using Seconds = std::chrono::duration<double>;

  TimePoint start_;
  Duration elapsed_;
  std::string label_;

 public:
  Timer(std::string _label);
  ~Timer();
};
}  // namespace mimic

#endif  // BASIC_TIMER_HH_