#ifndef BASIC_RANGE_HH_
#define BASIC_RANGE_HH_

#include <iterator>
#include <mimic/basic.h>

namespace mimic {

class Range {
  class RangeIterator {
    McInt index_;
    McInt step_;

   public:
    using iterator_category = std::bidirectional_iterator_tag;

   public:
    RangeIterator() : index_{0}, step_ {1} {}
    RangeIterator(McInt ind, McInt step) : index_{ind}, step_{step} {}
    RangeIterator(RangeIterator &&other)
        : index_{other.index_}, step_{other.step_} {}
    RangeIterator(RangeIterator const &other)
        : index_{other.index_}, step_{other.step_} {}

    RangeIterator &operator=(RangeIterator &&other) {
      index_ = other.index_;
      step_ = other.step_;
      return *this;
    }
    RangeIterator &operator=(RangeIterator const &other) {
      index_ = other.index_;
      step_ = other.step_;
      return *this;
    }

    McInt operator*() const { return index_; }

    bool operator==(RangeIterator const &rhs) const {
      return index_ == rhs.index_;
    };
    bool operator!=(RangeIterator const &rhs) const {
      return !operator==(rhs);
    }

    RangeIterator &operator++() {
      index_ += step_;
      return *this;
    }
    RangeIterator operator++(int) {
      auto ret = *this;
      ++(*this);
      return ret;
    }
    RangeIterator operator--() {
      index_ -= step_;
      return *this;
    }
    RangeIterator operator--(int) {
      auto ret = *this;
      ++(*this);
      return ret;
    }
  };

  RangeIterator start_;
  RangeIterator end_;

 public:
  using iterator = RangeIterator;        // NOLINT
  using const_iterator = const iterator; // NOLINT
  using difference_type = McInt;         // NOLINT

  Range(McInt start, McInt end, McInt step=1) :
      start_{start, step}, end_{end, step} {}
  Range(McInt end):
      start_{0, 1}, end_{end, 1} {}
  ~Range() = default;

  iterator       begin()        { return cbegin(); }
  iterator       end()          { return cend();   }
  const_iterator cbegin() const { return start_;   }
  const_iterator cend()   const { return end_;     }
};

}  // namespace mimic

#endif  // BASIC_RANGE_HH_