#ifndef BASIC_MATH_HH_
#define BASIC_MATH_HH_

#include <mimic/basic.h>
#include <Eigen/Dense>
#include <cmath>
#include <type_traits>
#include <numeric>
#include <utility>

#include "range.hh"
#include "eigen_types.hh"

namespace mimic {
namespace math {

template <typename Iter>
inline McFloat beta(Iter begin, Iter end) {
  McFloat numerator = std::accumulate(begin, end, 1.0f,
                                      [](McFloat accu, McFloat elem) {
                                        return std::tgamma(elem) * elem;
                                      });
  McFloat denominator =
      std::tgamma(std::accumulate(begin, end, 0.0f, std::plus<McFloat>()));
  return numerator / denominator;
}

inline McFloat lbeta(Eigen::VectorXf const& alpha) {
  Eigen::VectorXf temp(alpha.size());
  for (McFloat i = 0; i < alpha.size(); ++i) {
    temp[i] = std::lgamma(alpha[i]);
  }
  McFloat result = temp.sum();
  result -= std::lgamma(alpha.sum());
  return result;
}

/*! \brief Log Sum Exp
 *  https://stackoverflow.com/questions/45943750/calculating-log-sum-exp-function-in-c
 */
template <typename Iter>
typename std::iterator_traits<Iter>::value_type logSumExp(
    Iter begin, Iter end) {
  using VT = typename std::iterator_traits<Iter>::value_type;
  if (begin == end) return VT {};
  VT max_elem = *std::max_element(begin, end);
  VT sum = std::accumulate(begin, end, VT{},
                           [max_elem](VT a, VT b) {
                             return a + std::exp(b - max_elem);
                           });
  return max_elem + std::log(sum);
}

Eigen::VectorXf logSumExp(MatrixXfC const& value, McInt axis);

inline std::pair<MatrixXfC, McFloat> norm(MatrixXfC const& x) {
  McFloat z = logSumExp(x.data(), x.data() + x.size());
  MatrixXfC b = x.array() - z;
  return {b, z};
}

inline MatrixXfC ldot(MatrixXfC a, MatrixXfC b) {
  McFloat max_a = a.maxCoeff();
  McFloat max_b = b.maxCoeff();
  MatrixXfC a_e = Eigen::exp(a.array() - max_a);
  MatrixXfC b_e = Eigen::exp(b.array() - max_b);
  MatrixXfC c = a_e * b_e;
  c = c.array().log();
  c.array() += max_a + max_b;
  return c;
}

/*! \brief outer product in log domain. */
inline MatrixXfC louter(Eigen::VectorXf const& a,
                        Eigen::VectorXf const& b) {
  MatrixXfC res = MatrixXfC::Zero(a.rows(), b.rows());
  for (auto i : Range(a.rows())) {
    res.col(i).array() = b.array() + a(i);
  }
  return res;
}

}  // namespace math
}  // namespace mimic

#endif  // BASIC_MATH_HH_