#ifndef _ERROR_HH_
#define _ERROR_HH_

#include <exception>
#include <string>

namespace mimic {

class MimicError : public std::exception {
  std::string error_;

 public:
  explicit MimicError(std::string const& what_arg) : error_{what_arg} {}
  explicit MimicError(char const* what_arg) : error_{what_arg} {}

  virtual const char* what() const noexcept {  // NOLINT
    return error_.c_str();
  }
  virtual ~MimicError() = default;
};

class WarningError : public MimicError {
 public:
  explicit WarningError(std::string const& what_arg) : MimicError{what_arg} {}
  explicit WarningError(char const* what_arg) : MimicError{what_arg} {}
};

class FatalError : public MimicError {
 public:
  explicit FatalError(std::string const& what_arg) : MimicError{what_arg} {}
  explicit FatalError(char const* what_arg) : MimicError{what_arg} {}
};

class NotSupportedError : public MimicError {
  std::string msg_;

 public:
  explicit NotSupportedError(std::string const& feature,
                             std::string const& component) :
      MimicError{""},
      msg_{component + " doesn't support " + feature} {}
  explicit NotSupportedError(char const* feature,
                             char const* component) :
      NotSupportedError(std::string{feature}, std::string{component}) {}

  virtual const char* what() const noexcept override {
    return msg_.c_str();
  }

  virtual ~NotSupportedError() = default;
};

}  // namespace mimic

#endif  // _ERROR_HH_