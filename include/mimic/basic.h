#ifndef MIMIC_BASIC_H_
#define MIMIC_BASIC_H_

#include <cstdint>

namespace mimic {

using McInt    = int64_t;
using McFloat  = float;
using McDouble = double;

McFloat constexpr kEps = 1e-6f;

}  // namespace mimic

#endif  // MIMIC_BASIC_H_
