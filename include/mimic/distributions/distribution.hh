#ifndef DISTRIBUTIONS_DISTRIBUTION_HH_
#define DISTRIBUTIONS_DISTRIBUTION_HH_
#include <mimic/basic.h>

#include <vector>
#include <string>
#include <memory>
#include <Eigen/Dense>

#include <nih/logging.hh>
#include <nih/json.hh>
#include "mimic/basic/eigen_types.hh"

namespace mimic {
namespace dist {

class Distribution {
 public:
  enum DistKind {
    kExponential = 0,
    kDirichlet = 1,
    kMultinoulli = 2,
    kExponentialEnd
  };

 private:
  DistKind const kind_;

 public:
  Distribution(DistKind k) : kind_{k} {}
  virtual ~Distribution() = default;

  virtual DistKind getKind() const { return kind_; }
  virtual std::string getKindStr() const;

  virtual Eigen::VectorXf genSample() const = 0;
  virtual McFloat probability(Eigen::VectorXf const& x) const = 0;
  virtual McFloat logProbability(Eigen::VectorXf const& x) const = 0;

  virtual void save(nih::json::Json& handler) const = 0;
  virtual void load(nih::json::Json const& handler) = 0;

  static Distribution* create(std::string name);
};

class Exponential : public Distribution {
 protected:
  Exponential(DistKind k) : Distribution(k) {}

 public:
  virtual ~Exponential() = default;

  static bool isClassOf(const Distribution* d) {
    return
        d->getKind() >= kExponential &&
        d->getKind() <= kExponentialEnd;
  }

  virtual Eigen::VectorXf getNatural() const = 0;
  virtual void setNatural(Eigen::VectorXf natural) = 0;

  virtual std::unique_ptr<Distribution> genLogExpected() = 0;
  virtual MatrixXfC calcExpectedSufficient(
      Eigen::VectorXf const& obs, Eigen::VectorXf const& states_probs) const = 0;

  virtual std::shared_ptr<Exponential const> prior() const = 0;
  virtual std::shared_ptr<Exponential> prior()= 0;
};

// FIXME(trivialfis): entropy, cdf
#define ENSURE_METHODS_EXIST(distribution)                              \
  static __attribute__((unused)) const auto                             \
  __ ## distribution ## __mean__ = &distribution::mean;                 \
  static __attribute__((unused)) const auto                             \
  __ ## distribution ## __mode__ = &distribution::mode;                 \
  static __attribute__((unused)) const auto                             \
  __ ## distribution ## __variance__ = &distribution::variance;         \
  static __attribute__((unused)) const auto                             \
  __ ## distribution ## __covariance__ = &distribution::covariance;

}  // namespace dist
}  // namespace mimic

#endif  // DISTRIBUTIONS_DISTRIBUTION_HH_