#ifndef DISTRIBUTION_DIRICHLET_HH_
#define DISTRIBUTION_DIRICHLET_HH_

#include <mimic/basic.h>
#include <Eigen/Dense>
#include <random>
#include <vector>

#include "mimic/distributions/distribution.hh"

namespace mimic {
namespace dist {

class Dirichlet : public Exponential {
  Eigen::VectorXf concentrations_;
  std::shared_ptr<Exponential> prior_;

 public:
  Dirichlet();
  Dirichlet(Eigen::VectorXf const& alpha);

  static bool isClassOf(const Distribution* d) {
    return d->getKind() == kDirichlet;
  }

  // Distribution interface
  Eigen::VectorXf getParameters() const;
  McFloat probability(Eigen::VectorXf const& x) const override;
  McFloat logProbability(Eigen::VectorXf const& x) const override;
  void save(nih::json::Json& handler) const override;
  void load(nih::json::Json const& handler) override;

  // Exponential interface
  Eigen::VectorXf getNatural() const override;
  void setNatural(Eigen::VectorXf natural) override;
  MatrixXfC calcExpectedSufficient(
      Eigen::VectorXf const& obs, Eigen::VectorXf const& states_probs) const override;
  std::unique_ptr<Distribution> genLogExpected() override;
  std::shared_ptr<Exponential const> prior() const override {
    LOG(FATAL) << "Not implemented";
    // https://en.wikipedia.org/wiki/Dirichlet_distribution#Conjugate_prior_of_the_Dirichlet_distribution
    // http://cvsp.cs.ntua.gr/publications/jpubl+bchap/LefkimmiatisMaragosPapandreou_BayesianMultiscalePoissonIntensityEstimation_ieee-j-ip09.pdf
    return prior_;
  }
  std::shared_ptr<Exponential> prior() override {
    LOG(FATAL) << "Not implemented";
    return prior_;
  }

  // Interface for all distributions but not defined as virtual function
  void setParameters(Eigen::VectorXf params);
  Eigen::VectorXf genSample() const override;
  Eigen::VectorXf mean() const;
  Eigen::VectorXf mode() const;
  Eigen::VectorXf variance() const;
  MatrixXfC covariance() const;

  // Other
  McFloat calcKLDivergence(Exponential const* const other);
};

}  // namespace dist
}  // namespace mimic

#endif  // DISTRIBUTION_DIRICHLET_HH_