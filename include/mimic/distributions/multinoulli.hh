#ifndef DISTRIBUTIONS_MULTINOULLI_HH_
#define DISTRIBUTIONS_MULTINOULLI_HH_

#include <memory>
#include <Eigen/Dense>

#include "mimic/basic/cache.hh"
#include "distribution.hh"

namespace mimic {
namespace dist {

class Multinoulli : public Exponential {
 public:
  Multinoulli();
  Multinoulli(Eigen::VectorXf const& probs);
  static bool isClassOf(const Distribution* d) {
    return d->getKind() == kMultinoulli;
  }

  // Distribution interface
  Eigen::VectorXf getParameters() const;
  McFloat logProbability(Eigen::VectorXf const& x) const override;
  McFloat probability(Eigen::VectorXf const& x) const override;
  void save(nih::json::Json& handler) const override;
  void load(nih::json::Json const& handler) override;

  // Exponential interface
  Eigen::VectorXf getNatural() const override;
  void setNatural(Eigen::VectorXf natural) override;
  MatrixXfC calcExpectedSufficient(
      Eigen::VectorXf const& obs, Eigen::VectorXf const& states_probs) const override;
  std::unique_ptr<Distribution> genLogExpected() override;
  std::shared_ptr<Exponential const> prior() const override {
    return prior_;
  }
  std::shared_ptr<Exponential> prior() override {
    return prior_;
  }

  // Interface for all distributions but not defined as virtual function
  void setParameters(Eigen::VectorXf const& probs);
  Eigen::VectorXf genSample() const override;
  Eigen::VectorXf mean(McInt n) const;
  Eigen::VectorXf mode() const;
  Eigen::VectorXf variance() const;
  MatrixXfC covariance() const;

 private:
  Eigen::VectorXf probs_;
  std::unique_ptr<Cache<Eigen::VectorXf>> natural_cache_;

  std::shared_ptr<Exponential> prior_;
};

}  // namespace dist
}  // namespace mimic

#endif  // DISTRIBUTIONS_MULTINOULLI_HH_