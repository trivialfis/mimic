#ifndef PIPELINE_SESSION_HH_
#define PIPELINE_SESSION_HH_

#include <ctime>
#include <set>

#include <nih/strings.hh>

#include "mimic/basic.h"
#include "mimic/basic/strings.hh"
#include "mimic/basic/FileSystem.hh"
#include "mimic/models/model.hh"

namespace mimic {
namespace pipeline {

static std::string timestr() {
  std::time_t now = std::time(0);
  char* dt = std::ctime(&now);
  std::string dt_str {dt};
  std::vector<std::string> res;
  nih::split(res, dt_str, nih::isSpace);
  dt_str.clear();
  for (auto v : res) {
    dt_str += v;
    dt_str += '-';
  }
  dt_str = dt_str.substr(0, dt_str.size()-1);  // remove last '-'
  return dt_str;
}

struct Parameter {
  model::Model::ModelKind model;
  u8string input_filename;
  McInt iterations;
  McInt seed;
  bool abort;

  McInt vbhmm_subchain_len_;
  McInt vbhmm_buffer_len_;
  McInt vbhmm_num_states_;

  McFloat learning_rate_;

  Path io_path_;

  bool is_predict_;

  bool isSeeded() const {
    return seed != std::numeric_limits<McInt>::min();
  }

  Parameter() :
      model { model::Model::kUnknown },
      iterations { 1 },
      seed { std::numeric_limits<McInt>::min() },
      abort { false },
      vbhmm_subchain_len_ {384},
      vbhmm_buffer_len_ {32},
      vbhmm_num_states_ {12},
      learning_rate_ { 0.5 },
      io_path_ {"session-" + timestr() + ".json"},
      is_predict_ {false}
  {}
};

class Operation {
 public:
  Operation() = default;
  virtual ~Operation() = default;

  virtual void execute() = 0;
  virtual void save(nih::json::Json& handle) const = 0;
  virtual void load(nih::json::Json const& handle) = 0;

  static std::vector<std::unique_ptr<Operation>> create(Parameter params);
};

class Session {
  std::vector<std::unique_ptr<Operation>> ops_;
  Parameter params_;

 public:
  Session() = default;
  Session(std::vector<std::unique_ptr<Operation>>& ops) {
    take(ops);
  }

  void setParameters(Parameter params) {
    params_ = params;
  }

  void take(std::vector<std::unique_ptr<Operation>>& ops) {
    for (auto ind : Range(ops.size())) {
      ops_.push_back(std::move(ops[ind]));
    }
  }

  void run() {
    if (params_.is_predict_) {
      load();
    }
    for (auto& o : ops_) {
      o->execute();
    }
    if (!params_.is_predict_) {
      save();
    }
  }

  void save();
  void load();
};

}  // namespace pipeline
}  // namespace mimic

#endif