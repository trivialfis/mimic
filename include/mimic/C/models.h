#ifndef C_MODELS_H_
#define C_MODELS_H_

#include "mimic/kiwi.h"

KiwiAPI Kiwi_ErrorCode
Kiwi_modelStep(Kiwi_model model, Kiwi_sequence seq);

KiwiAPI Kiwi_ErrorCode
Kiwi_modelMeasure(Kiwi_model handle, Kiwi_sequence seq, float* score);

KiwiAPI
Kiwi_ErrorCode Kiwi_modelSave(Kiwi_model p_model, char* cpath);
KiwiAPI
Kiwi_ErrorCode Kiwi_modelLoad(Kiwi_model p_model, char* cpath);

KiwiAPI Kiwi_model
Kiwi_VBHmmInit(int n_states, int n_symbols, float rate,
               int subchain_length, int buffer_length, int batch_size,
               int full_step);

KiwiAPI Kiwi_model Kiwi_HmmNew();
KiwiAPI Kiwi_model Kiwi_HmmInit(int nstates, int nsymbols);

KiwiAPI Kiwi_model Kiwi_HistNew();
KiwiAPI Kiwi_model Kiwi_HistInit();

#endif  // C_MODELS_H_