#ifndef C_LOGGING_H_
#define C_LOGGING_H_

#include "mimic/kiwi.h"

KiwiAPI Kiwi_ErrorCode Kiwi_log(char const* msg, int verbosity);
KiwiAPI Kiwi_ErrorCode Kiwi_logDefault(char const* msg);

KiwiAPI Kiwi_ErrorCode Kiwi_setLogVerbosity(char const* msg);

#endif  // C_LOGGING_H_