#ifndef DATA_TENSOR_HH_
#define DATA_TENSOR_HH_

#include <mimic/basic.h>

namespace mimic {
namespace yat {

class TensorImpl;

class Tensor {
 public:
  enum TensorType {
    Int,
    Float,
    Double
  };

 private:
  TensorImpl* p_impl_;
};

}
}

#endif  // DATA_TENSOR_HH_