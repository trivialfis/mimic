#ifndef _DATA_MATRIX_HH_
#define _DATA_MATRIX_HH_

#include <vector>
#include <string>

#include "mimic/basic.h"

namespace mimic {

struct Dim {
  Dim() : rows_{0}, cols_{0} {}
  Dim(McInt _rows, McInt _cols) : rows_{_rows}, cols_{_cols} {}

  McInt Rows() const { return rows_; }
  McInt Cols() const { return cols_; }

 private:
  McInt rows_;
  McInt cols_;
};

template <typename T>
class Matrix {
  Dim dimension_;

 public:
  Matrix(Dim _dim) : dimension_{_dim} {}

  virtual McInt Rows() const { return dimension_.Rows(); }
  virtual McInt Cols() const { return dimension_.Cols(); }
};

template <typename T>
class CsrMatrix : public Matrix<T> {
 private:
  std::vector<T> values_;
  std::vector<McInt> row_ptrs_;
  std::vector<McInt> col_ptrs_;

  void Forward();
  void Backward();

 public:
  CsrMatrix() = default;
  CsrMatrix(Dim _dim) : Matrix<T>(_dim) {}
  CsrMatrix(Dim _dim,
            std::vector<T>&& _values,
            std::vector<McInt>&& _row_ptrs,
            std::vector<McInt>&& _col_ptrs)
      : Matrix<T>(_dim),
        values_{std::move(_values)},
        row_ptrs_{std::move(_row_ptrs)},
        col_ptrs_{std::move(_col_ptrs)}{}

  bool operator==(CsrMatrix<T> const& other) const;

  std::vector<T> const& GetValues() const;
  std::vector<McInt> const& GetRowPtrs() const;
  std::vector<McInt> const& GetColPtrs() const;
};

}  // namespace mimic

#endif // _DATA_MATRIX_HH_