#ifndef _DATA_PARSERS_HH_
#define _DATA_PARSERS_HH_

#include <Eigen/Dense>

#include <fstream>
#include <memory>

#include <vector>
#include <set>

#include "Matrix.hh"
#include "mimic/basic/FileSystem.hh"

namespace mimic {

class DataSource {
  std::string uri;

 public:
  DataSource(std::string _uri) : uri{_uri} {}
  virtual std::string const& GetUri() const;
};

template <typename T>
class Parser {
 public:
  Parser(){}
  virtual Matrix<T>* Parse(std::ifstream& fin) = 0;
};

template <typename T>
class DenseParser : Parser<T> {
 public:
  DenseParser(){}
  virtual Matrix<T>* Parse(std::ifstream& fin) override;
};

template <typename T, typename ParsePolicy>
class MatrixLoader : public ParsePolicy {
  DataSource source;

 public:
  MatrixLoader() : source {""} {}
  MatrixLoader(DataSource const& src) : source{src} {}

  virtual Matrix<T>* Load() {
    std::ifstream fin { source.GetUri() };
    if (!fin) {
      throw std::runtime_error("File: " + source.GetUri() + " not found");
    }
    Matrix<T>* result = ParsePolicy::Parse(fin);
    fin.close();
    return result;
  }
};

using DenseMatrixLoader = MatrixLoader<McFloat, DenseParser<McFloat>>;

Eigen::VectorXf LoadToEigenVector(Path source);

class DataLoader {
 public:
  std::vector<McFloat> load(Path path) const;
  void load(Path path, std::vector<McFloat>* dst) const;
};

}  // namespace mimic

#endif  // _DATA_PARSERS_HH_