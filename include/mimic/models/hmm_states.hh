#ifndef MODELS_HMM_STATES_HH_
#define MODELS_HMM_STATES_HH_

#include <memory>
#include <mimic/basic.h>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "nih/json.hh"

#include "mimic/distributions/distribution.hh"
#include "mimic/basic/eigen_types.hh"
#include "mimic/models/model.hh"

namespace mimic {
namespace model {

// States including both observed and hidden. Currently used by vbhmm,
// but should be later merged with plain HMM.
class DistHiddenMarkovModel : public model::Model {
 public:
  DistHiddenMarkovModel();
  DistHiddenMarkovModel(McInt k_states, MatrixXfC transitions,
            Eigen::VectorXf pi,
            std::vector<std::shared_ptr<dist::Distribution>> emits,
            MatrixXfR const& obs);

  Eigen::Tensor<McFloat, 3> const& getGammas() const { return gammas_; }
  MatrixXfC const& getTransition() const { return transitions_; }
  Eigen::VectorXf const& getStart() const { return pi_; };
  McInt getNumHidden() const { return k_states_; }

  std::vector<std::shared_ptr<dist::Distribution>>
  getEmission() const { return emits_; }

  MatrixXfC calcLocalTransition(McInt a, McInt b) const;
  MatrixXfC calcTransition() const;
  Eigen::VectorXf calcStart() const;

  McFloat logLikelihood(MatrixXfR const& obs) const;

  void eStepRowSubchain(McInt row_id, McInt start, McInt end, McInt buf_size, MatrixXfR const& obs);
  void eStep(MatrixXfR const& obs);
  void eStepRow(McInt row_id, MatrixXfR const& obs);

  void mStep(MatrixXfR const& obs);

  void step(MatrixXfR const& obs) override;
  McFloat measure(MatrixXfR const& obs) const override;

  friend std::ostream& operator<<(std::ostream& os, DistHiddenMarkovModel state) {
    os << "transitions_: " << state.transitions_ << std::endl
       << "pi_: " << state.pi_ << std::endl
       << "gammas_: " << state.gammas_ << std::endl
       << "xi_: " << state.xi_;
    return os;
  }

  void save(nih::json::Json& handle) const override;
  void load(nih::json::Json const& handle) override;

 protected:
  std::pair<MatrixXfC, Eigen::VectorXf> forward(
      Eigen::VectorXf const& obs, MatrixXfC const& log_emit) const;
  MatrixXfC backward(Eigen::VectorXf obs, MatrixXfC const& log_emit);

  void updateTransition();
  void updateStart();

  McInt k_states_;
  MatrixXfC transitions_;
  Eigen::VectorXf pi_;
  std::vector<std::shared_ptr<dist::Distribution>> emits_;

  Eigen::Tensor<McFloat, 3> gammas_;
  Eigen::Tensor<McFloat, 4> xi_;
};

}  // namespace model
}  // namespace mimic
#endif  // MODELS_HMM_STATES_HH_