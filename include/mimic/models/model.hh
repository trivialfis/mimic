#ifndef MODELS_MODEL_HH_
#define MODELS_MODEL_HH_

#include <nih/json.hh>

#include "mimic/basic.h"
#include "mimic/basic/eigen_types.hh"

namespace mimic {
namespace model {

class Model {
 public:
  enum ModelKind {
    kUnknown = 0,
    kBaseline = 1,
    kHMM = 2,
    kDistHMM = 3,
    kVBHMM = 4,
    kFullVBHMM = 5
  };

  virtual ModelKind getKind() const;
  virtual std::string getName() const;

  static Model* create(std::string name);
  static Model* create(ModelKind kind);

  static std::string toName(Model::ModelKind kind);
  static ModelKind toKind(std::string name);

  virtual void step(MatrixXfR const& obs) = 0;
  virtual McFloat measure(MatrixXfR const& obs) const = 0;
  virtual ~Model() {};

  // FIXME(trivialfis): Make these pure virtual
  virtual void save(nih::json::Json& handle) const {
    LOG(FATAL) << "Not Implemented";
  }
  virtual void load(nih::json::Json const& handle) {
    LOG(FATAL) << "Not Implemented";
  }

 private:
  ModelKind kind_;

 protected:
  Model(ModelKind kind) : kind_{kind} {}
};

}  // namespace model
}  // namespace mimic

#endif  // MODELS_MODEL_HH_