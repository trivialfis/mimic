#ifndef MODELS_VBHMM_HH_
#define MODELS_VBHMM_HH_

#include <mimic/basic.h>
#include <Eigen/Dense>
#include <memory>

#include "nih/json.hh"

#include "mimic/models/model.hh"
#include "mimic/models/hmm_states.hh"
#include "mimic/distributions/distribution.hh"
#include "mimic/distributions/dirichlet.hh"
#include "mimic/basic/eigen_types.hh"

namespace mimic {
namespace model {

class VBHMM : public Model {
 private:
  static McFloat constexpr kDefaultConcentration_ = 3.0f;

 protected:
  McInt num_states_;
  McFloat learning_rate_;
  McInt subchain_length_;
  McInt buffer_length_;
  McInt batch_size_;
  bool is_full_step_;

  std::shared_ptr<dist::Dirichlet> u_a_;
  std::shared_ptr<dist::Dirichlet> u_pi_;
  std::vector<std::shared_ptr<dist::Dirichlet>> u_d_;
  std::shared_ptr<dist::Dirichlet> w_pi;
  std::vector<std::shared_ptr<dist::Dirichlet>> w_a_;

  /*! \brief k distributions governing emission */
  std::vector<std::shared_ptr<dist::Exponential>> emissions_;

  DistHiddenMarkovModel states_;

  bool initialized;

 protected:
  void initializeDists();
  DistHiddenMarkovModel generateHMM(bool local, MatrixXfR const& obs) const;
  /*! \brief Generate a range with length subchain_size. */
  std::pair<McInt, McInt> genRange(
      McInt obs_size, McInt buf_size, McInt subchain_size);

  void fullVBStep(MatrixXfR const& obs);
  void batchSVIStep(MatrixXfR const& obs);
  McFloat elbo(MatrixXfR const& obs) const;

 public:
  VBHMM();
  VBHMM(McInt n_states, McInt n_syms,
        McFloat rate,
        McInt subchain_length=0, McInt buffer_length=0,
        McInt batch_size=1,
        bool full_step=true);
  VBHMM(McInt n_states,
        std::shared_ptr<dist::Dirichlet> _u_a,
        std::shared_ptr<dist::Dirichlet> _u_pi,
        std::vector<std::shared_ptr<dist::Dirichlet>> _u_d,
        std::vector<std::shared_ptr<dist::Exponential>> _emiss);
  VBHMM(VBHMM const& other);

  std::map<std::string,
           std::vector<std::shared_ptr<dist::Exponential const>>>
  getAllDistributions() const;

  ~VBHMM() = default;

  void step(MatrixXfR const& obs) override;
  McFloat measure(MatrixXfR const& obs) const override;

  void save(nih::json::Json& handle) const override;
  void load(nih::json::Json const& handle) override;
};

}  // namespace model
}  // namespace mimic

#endif  // MODELS_VBHMM_HH_