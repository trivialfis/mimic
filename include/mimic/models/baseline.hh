#ifndef EXPERIMENTS_BASELINE_HH_
#define EXPERIMENTS_BASELINE_HH_

#include "mimic/data/Matrix.hh"
#include "mimic/data/Loader.hh"
#include "mimic/models/model.hh"
#include "mimic/basic.h"

#include <memory>
#include <limits>
#include <vector>
#include <set>
#include <string>

namespace mimic {
namespace model {

class TracesTable {
 public:
  using Call_t = size_t;
  using Trace = std::vector<Call_t>;
  using TracesSet = std::set<Trace>;
  using Table = std::vector<TracesSet>;

  /*! \brief Not a call, in the spirit of not a number (NAN). */
  static Call_t constexpr NAC = std::numeric_limits<Call_t>::max();
  /*! \brief Number of existing kernel calls. */
  static constexpr size_t kNCalls = 256;

 private:
  /*! \brief Total system calls in trace. */
  McInt trace_len_;
  McInt lookahead_len_;
  McInt total_possible_mismatches_;

  Table traces_table_;

  McInt totalPossibleMismatches () const;

 public:
  TracesTable() :
      trace_len_{0}, lookahead_len_{3}, total_possible_mismatches_{0},
      traces_table_{kNCalls} {}
  TracesTable(McInt trace_length, McInt lookhead) :
      trace_len_{trace_length}, lookahead_len_(lookhead),
      total_possible_mismatches_{0},
      traces_table_{kNCalls} {}
  ~TracesTable() = default;

  void parse(Eigen::VectorXf const& sequence);
  void merge(TracesTable const& table);
  TracesSet const& tracesSetAt(Call_t const call) const {
    return traces_table_.at(call);
  }

  std::vector<TracesTable::TracesSet>& getTable();
  size_t getLookAheadLength() const { return lookahead_len_; }
  size_t getTraceLength() const { return trace_len_; }
  size_t getTotalPossibleMismatches() const;

  void clear() { traces_table_.clear(); traces_table_.shrink_to_fit(); }
};

class BaseLineDetector : public Model {
 protected:
  TracesTable table_;

  size_t calcMismatch(TracesTable const& other) const;

 public:
  BaseLineDetector();

  void step(MatrixXfR const& sequence) override;
  McFloat measure(MatrixXfR const& sequence) const override;
};

}  // namespace model
}  // namespace mimic

#endif  // EXPERIMENTS_BASELINE_HH_