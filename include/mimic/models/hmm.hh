#ifndef HMM_HH_
#define HMM_HH_

#include <iostream>
#include <string>
#include <utility>

#include <Eigen/Sparse>
#include <Eigen/Dense>

#include "mimic/models/model.hh"
#include "mimic/data/Matrix.hh"
#include "mimic/basic/eigen_types.hh"

namespace mimic {
namespace model {

/*! \brief Implementation of Hidden Markov Model. */
class HiddenMarkovModel : public Model {
 public:
  struct Parameter {
    McInt n_states;
    McInt n_symbols;

   public:
    Parameter();
    Parameter(McInt n_states, McInt n_symbols);
    void SetNumStates(McInt val);
    void SetNumSymbols(McInt val);
  };
  MatrixXfC gpuForward(Eigen::VectorXf const& sequence) const;

 protected:
  Parameter param_;

  MatrixXfR transition_;
  MatrixXfC emission_;
  Eigen::VectorXf scales_;
  Eigen::VectorXf initial_;

  bool initialized_;
  static McInt constexpr kDefaultMaxIters = 256;

  /*! \brief Calculate the forward(alpha) pass and the scale factors. */
  MatrixXfC forward(Eigen::VectorXf const& sequence);
  MatrixXfC forward(Eigen::VectorXf const& sequence,
                    Eigen::VectorXf& scales) const;

  MatrixXfC backward(Eigen::VectorXf const& sequence) const;
  std::pair<MatrixXfC, std::vector<MatrixXfC>> estimateStates(
      Eigen::VectorXf const& sequence);
  void reestimate(Eigen::VectorXf const& sequence);

 public:
  HiddenMarkovModel();
  HiddenMarkovModel(Parameter _param);
  HiddenMarkovModel(McInt n_states, McInt n_symbols);
  virtual ~HiddenMarkovModel() = default;
  void finalize();

  void step(MatrixXfR const& sequence) override;
  McFloat measure(MatrixXfR const& sequence) const override;

  friend std::ostream& operator<<(
      std::ostream& os, HiddenMarkovModel const& model) {
    os << "Transition: " << model.transition_
       << "Emission: " << model.emission_
       << "Initial: "  <<  model.initial_
       << "Scales: " << model.scales_;
    return os;
  }
};

}      // namespace model
}      // namespace mimic
#endif  // HMM_HH_