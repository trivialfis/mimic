import ctypes
import os
import numpy as np
libdir = ''


class MimicError(ValueError):
    pass


def c_str(s: str):
    return ctypes.c_char_p(s.encode('utf-8'))


class LibNih:
    handle = ctypes.cdll.LoadLibrary(os.path.join(
        libdir,
        './3-parties/nih/libnih.so'))


class LibMimic:
    handle = ctypes.cdll.LoadLibrary(os.path.join(
        libdir,
        './libmimic.so'))

    @classmethod
    def _check(self, v: int):
        if v == 1:
            raise Warning
        if v == 2:
            raise MimicError

    @classmethod
    def global_set_param(self, key: str, val: str):
        self.handle.Kiwi_globalSetParam(c_str(key), c_str(val))

    @classmethod
    def create_model(self, name: str):
        return self.handle.Kiwi_createModel(c_str(name))

    @classmethod
    def destroy_model(self, model):
        self._check(self.handle.Kiwi_destroyModel(model))

    @classmethod
    def log(self, msg: str, verbosity: int = None):
        if verbosity:
            self._check(self.handle.Kiwi_log(c_str(msg),
                                             ctypes.c_int(verbosity)))
        else:
            self._check(self.handle.Kiwi_logDefault(c_str(msg)))

    @classmethod
    def load_file(self, path: str):
        return self.handle.Kiwi_loadFile(c_str(path))

    @classmethod
    def destroy_sequence(self, seq):
        self._check(self.handle.Kiwi_destroySequence(seq))

    @classmethod
    def model_step(self, model, seq):
        self._check(self.handle.Kiwi_modelStep(model, seq))

    @classmethod
    def vbhmm_init(self, n_states, n_symbols, rate, subchain_length,
                   buffer_length, batch_size, full_step):
        return self.handle.Kiwi_VBHmmInit(
            n_states, n_symbols, ctypes.c_float(rate),
            subchain_length, buffer_length,
            batch_size, full_step)

    @classmethod
    def hmm_init(self, n_states, n_symbols):
        return self.handle.Kiwi_HmmInit(ctypes.c_int(n_states),
                                        ctypes.c_int(n_symbols))

    @classmethod
    def model_measure(self, model, seq):
        score: float = 0
        score = ctypes.c_float(score)
        self._check(self.handle.Kiwi_modelMeasure(model, seq, ctypes.byref(score)))
        return score.value

    @classmethod
    def mode_save(self, model, path: str):
        self._check(self.handle.Kiwi_modelSave(model, c_str(path)))

    @classmethod
    def model_load(self, model, path: str):
        self._check(self.handle.Kiwi_modelLoad(model, c_str(path)))


@property
def global_variables():
    pass


@global_variables.setter
def global_variables(dictionary: dict):
    for k, v in dictionary.items():
        LibMimic.global_set_param(k, v)


err = LibMimic.global_set_param('verbosity', 'Debug')

model, name = LibMimic.vbhmm_init(
    n_states=16,
    n_symbols=349,
    rate=0.06, subchain_length=128,
    buffer_length=32,
    batch_size=1, full_step=0), 'VBHMM'
# LibMimic.destroy_model(model)
# model, name = LibMimic.hmm_init(n_states=14, n_symbols=349), 'HMM'

sequence = LibMimic.load_file(
    '../../../datasets/ADFA/ADFA-LD/Training_Data_Master/')
ITERS = 314

previous_score = -np.infty
scores = []
for i in range(0, ITERS):
    LibMimic.model_step(model, sequence)
    score = LibMimic.model_measure(model, sequence)
    if score > previous_score:
        previous_score = score
    print('iter:', i, 'score:', score)
    scores.append(score)

with open('scores.txt', 'w') as fd:
    print(scores)
    print(scores, file=fd)
LibMimic.mode_save(model, name + '.json')
LibMimic.destroy_model(model)
LibMimic.destroy_sequence(sequence)