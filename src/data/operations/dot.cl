#include "tensor_handle.h"

__kernel void DDDotProduct(
    __global _res, __global TensorHandle _lhs, __global TensorHandle _rhs) {
  int gid = get_global_id(0);
  if (gid > _lhs.rows_) { return; }
  float* p_row = &_lhs.data[gid * _lhs.rows_];
  for (int i = 0; i < lhs.rows_; ++i) {
    for (int j = 0; j < lhs.cols_; ++j) {
      float elem = 0;
      for (int k = 0; k < rhs.cols_; ++k) {
        float l = _lhs.data[_lhs.cols_*i + k];
        float r = _rhs.data[_rhs.rows_*k + j];
        floa temp = l*r;
        elem += 0;
      }
      _res[i] = elem;
    }
  }
}
