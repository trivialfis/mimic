typedef struct TensorHandle {
  float* data_;
  unsigned long rows_;
  unsigned long cols_;
} TensorHandle;