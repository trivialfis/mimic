#include "mimic/data/Matrix.hh"
#include <algorithm>

namespace mimic {

template <typename T>
bool CsrMatrix<T>::operator==(CsrMatrix<T> const& other) const {
  bool same_size = other.values_.size() == values_.size() &&
                   other.row_ptrs_.size() == row_ptrs_.size() &&
                   other.col_ptrs_.size() == col_ptrs_.size();
  if (!same_size) return false;
  return
      Matrix<T>::Rows() == other.Rows() && Matrix<T>::Cols() == other.Cols() &&
      std::equal(row_ptrs_.cbegin(), row_ptrs_.cend(),
                 other.row_ptrs_.cbegin()) &&
      std::equal(col_ptrs_.cbegin(), col_ptrs_.cend(),
                 other.col_ptrs_.cbegin()) &&
      std::equal(values_.cbegin(), values_.cend(),
                 other.values_.cbegin());
}

template <typename T>
std::vector<T> const& CsrMatrix<T>::GetValues() const {
  return values_;
}
template <typename T>
std::vector<McInt> const& CsrMatrix<T>::GetRowPtrs() const {
  return row_ptrs_;
}
template <typename T>
std::vector<McInt> const& CsrMatrix<T>::GetColPtrs() const {
  return col_ptrs_;
}

#define DefineCsrMatrix(T)                                              \
  template class CsrMatrix<T>;                                          \
  // template bool CsrMatrix<T>::operator==(CsrMatrix<T> const& other) const;

DefineCsrMatrix(McFloat)
DefineCsrMatrix(McDouble)
DefineCsrMatrix(McInt)

}  // namespace mimic