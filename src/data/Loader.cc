#include <algorithm>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <numeric>
#include <iostream>
#include <memory>

#include <nih/logging.hh>

#include "mimic/kiwi.h"

#include <boost/filesystem.hpp>

#include "mimic/basic/strings.hh"
#include "mimic/basic/eigen_types.hh"

#include "mimic/data/Loader.hh"

#include "../C/api_errors.h"

namespace mimic {

std::string const& DataSource::GetUri() const {
  return uri;
}

template <typename T>
Matrix<T>* DenseParser<T>::Parse(std::ifstream& fin) {
  std::string content;
  fin.seekg(0, std::ios::end);
  content.resize(fin.tellg());
  fin.seekg(0, std::ios::beg);
  fin.read(&content[0], content.size());

  std::vector<std::string> splited;
  Split(splited, content, [](char c) { return c == ' '; });

  content.clear();  // No longer needed.

  std::vector<T> data;
  for (auto& s : splited) {
    T res = ston<McDouble>(s);
    data.push_back(res);
  }

  std::vector<McInt> col_ptrs( data.size() );
  std::iota(col_ptrs.begin(), col_ptrs.end(), 0);

  Matrix<T>* result {
    reinterpret_cast<Matrix<T>*>(
        new CsrMatrix<T> {
          Dim(1, col_ptrs.size()),
          std::move(data),
          std::move(std::vector<McInt>{0}),
          std::move(col_ptrs) })};
  return result;
}

#define DefineDenseParser(T)                                           \
  template Matrix<T>* DenseParser<T>::Parse(std::ifstream& fin);       \

DefineDenseParser(McFloat)
DefineDenseParser(McDouble)
DefineDenseParser(McInt)

std::vector<McFloat> loadFile(u8string uri) {
  std::ifstream fin(uri.cpp_str());
  if (!fin) {
    LOG(FATAL) << "Opening " << uri << " failed.";
  }
  std::string content;
  fin.seekg(0, std::ios::end);
  content.resize(fin.tellg());
  fin.seekg(0, std::ios::beg);
  fin.read(&content[0], content.size());
  fin.close();

  std::vector<std::string> splited;
  mimic::Split(splited, content, [](char c) { return c == ' '; });

  content.clear();  // No longer needed.
  content.shrink_to_fit();

  std::vector<mimic::McFloat> converted (splited.size());
  for (size_t i = 0; i < splited.size(); ++i) {
    converted[i] = mimic::ston<mimic::McFloat>(splited[i]);
  }

  splited.clear();
  splited.shrink_to_fit();

  return converted;
}

class FileLoader {
 public:
  void load(std::vector<McFloat>* dst, Path const& uri) const;
};

void FileLoader::load(std::vector<McFloat> *dst, const Path& uri) const {
  std::ifstream fin(uri.str().cpp_str());
  if (!fin) {
    LOG(FATAL) << "Opening " << uri << " failed.";
  }
  std::string content;
  fin.seekg(0, std::ios::end);
  content.resize(fin.tellg());
  fin.seekg(0, std::ios::beg);
  fin.read(&content[0], content.size());
  fin.close();

  std::vector<std::string> splited;
  mimic::Split(splited, content, [](char c) { return c == ' '; });

  content.clear();  // No longer needed.
  content.shrink_to_fit();

  dst->resize(splited.size());
  for (size_t i = 0; i < splited.size(); ++i) {
    (*dst)[i] = mimic::ston<mimic::McFloat>(splited[i]);
  }
}

class DirectoryLoader {
 public:
  void load(std::vector<McFloat>* content, Path const& dir) const;
};

void DirectoryLoader::load(std::vector<McFloat>* dst, Path const& dir) const {
  namespace bfs = boost::filesystem;
  if (!dir.exists()) {
    LOG(USER_E) << "Directory: " << dir.str() << " doesn't exist";
  }
  bfs::directory_iterator end_iter;

  for (bfs::directory_iterator iter(dir.str().cpp_str());
       iter != end_iter; ++iter) {
    if (bfs::is_directory(*iter)) {
      continue;
    }
    std::ifstream fin(iter->path().string());
    try {
      std::vector<McFloat> converted = loadFile(iter->path().string());
      dst->reserve(dst->size() + converted.size());
      for (auto v : converted) {
        dst->emplace_back(v);
      }
    } catch(std::exception& e) {
      LOG(FATAL) << "Loading file: " << iter->path().filename()
                 << " failed with: " << e.what();
    }
  }
}

std::vector<McFloat> DataLoader::load(Path path) const {
  namespace bfs = boost::filesystem;
  std::vector<McFloat> content;
  if (bfs::is_directory(path.str().cpp_str())) {
    LOG(USER) << "Loading from directory: " << path.str();
    DirectoryLoader loader;
    loader.load(&content, path);
  } else {
    LOG(USER) << "Loading from file: " << path.str();
    content = loadFile(path.str());
  }
  return content;
}

void DataLoader::load(Path path, std::vector<McFloat>* dst) const {
  namespace bfs = boost::filesystem;
  std::vector<McFloat> content;
  if (bfs::is_directory(path.str().cpp_str())) {
    LOG(USER) << "Loading from directory: " << path.str();
    DirectoryLoader loader;
    loader.load(dst, path);
  } else {
    LOG(USER) << "Loading from file: " << path.str();
    FileLoader loader;
    loader.load(dst, path);
  }
}

Eigen::VectorXf LoadToEigenVector(Path source) {
  DataLoader loader;
  std::vector<McFloat> converted { loader.load( source) };
  Eigen::VectorXf result(Eigen::VectorXf::Map(converted.data(), converted.size()));
  return result;
}

}  // namespace mimic

KiwiAPI Kiwi_sequence Kiwi_loadFile(char* dirpath) {
  mimic::Path path {dirpath};
  std::vector<mimic::McFloat> converted;
  mimic::DataLoader loader;
  loader.load(path, &converted);
  mimic::MatrixXfR* result = new mimic::MatrixXfR(1, converted.size());
  std::copy(converted.data(), converted.data() + converted.size(), result->data());
  return reinterpret_cast<Kiwi_sequence>(result);
}

KiwiAPI Kiwi_ErrorCode Kiwi_destroySequence(Kiwi_sequence seq) {
  C_API_BEG()
  {
    mimic::MatrixXfR* vec = reinterpret_cast<mimic::MatrixXfR*>(seq);
    if (seq) {
      delete vec;
    } else {
      LOG(FATAL) << "Null sequence";
    }
  }
  C_API_END()
}