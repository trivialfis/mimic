#include <fstream>
#include <utility>

#include "mimic/basic/eigen_types.hh"
#include "mimic/data/Loader.hh"
#include "mimic/basic/random.hh"

#include "mimic/pipeline/session.hh"
#include "mimic/models/model.hh"
#include "mimic/models/baseline.hh"
#include "mimic/models/hmm_states.hh"
#include "mimic/models/vbhmm.hh"
#include "mimic/models/hmm.hh"

namespace mimic {
namespace pipeline {

class TrainOp : public Operation {
 private:
  std::shared_ptr<mimic::model::Model> model_;
  Parameter params_;
  mimic::MatrixXfR obs_;
  std::string name;

 public:
  TrainOp(Parameter const& param) : params_{ param }, name { "Train" } {
    Eigen::VectorXf sequence_vec =
        mimic::LoadToEigenVector(params_.input_filename);
    mimic::MatrixXfR input (1, sequence_vec.size());
    input.row(0) = sequence_vec;
    switch (params_.model) {
      case mimic::model::Model::kVBHMM:
        model_.reset(new mimic::model::VBHMM(
            params_.vbhmm_num_states_,
            349,
            params_.learning_rate_,
            params_.vbhmm_subchain_len_,
            params_.vbhmm_buffer_len_,
            1, false));
        break;
      case mimic::model::Model::kFullVBHMM:
        model_.reset(new mimic::model::VBHMM(
            params_.vbhmm_num_states_, 349, 0.5));
        break;
      case mimic::model::Model::kDistHMM:
        model_.reset(new mimic::model::DistHiddenMarkovModel());
        break;
      case mimic::model::Model::kHMM:
        model_.reset(new mimic::model::HiddenMarkovModel());
        break;
      case mimic::model::Model::kBaseline:
        model_.reset(new mimic::model::BaseLineDetector());
        break;
      default:
        LOG(FATAL) << "Unknown model";
    }
    obs_ = input;
  }
  void execute() override {
    RandomContext context;
    if (params_.isSeeded()) {
      RandomContext c (params_.seed);
      context = std::move(c);
    }

    mimic::McInt iterations = params_.iterations;

    for (auto iter : mimic::Range(iterations)) {
      model_->step(obs_);
      McFloat score = model_->measure(obs_);
      LOG(USER) << "It:" << iter << ", Score: " << score;
    }
  }

  void save(nih::json::Json& handle) const override {
    std::string model_name = model_->getName();
    handle["model"] = model_name;
    handle[model_name] = nih::json::Object();
    model_->save(handle[model_name]);
  };

  void load(nih::json::Json const& handle) override {
    namespace js = nih::json;
    std::string model_name = js::get<js::String>(handle["model"]);
    model_->load(handle[model_name]);
  }
};

class PredictOp : public Operation {
  std::shared_ptr<mimic::model::Model> model_;
  Parameter params_;
  mimic::MatrixXfR obs_;

 public:
  PredictOp(Parameter param) : params_{std::move(param)} {
    Eigen::VectorXf sequence_vec =
        mimic::LoadToEigenVector(params_.input_filename);
    mimic::MatrixXfR input (1, sequence_vec.size());
    input.row(0) = sequence_vec;
    obs_ = input;
  }

  void execute() override {
    RandomContext context;
    if (params_.isSeeded()) {
      RandomContext c (params_.seed);
      context = std::move(c);
    }

    McFloat score = model_->measure(obs_);
    LOG(USER) << "Score: " << score;
  }

  void save(nih::json::Json& handle) const override {
    // Do nothing for prediction.
  }
  void load(nih::json::Json const& handle) override {
    namespace js = nih::json;
    std::string model_name = js::get<js::String>(handle["model"]);
    model_.reset(model::Model::create(model_name));
    model_->load(handle[model_name]);
  }
};

void Session::save() {
  LOG(USER) << R"(Saving session to ")" + params_.io_path_.str().cpp_str() + "\"";
  nih::json::Json handle { nih::json::Object() };
  for (auto& o : ops_) {
    o->save(handle);
  }
  std::ofstream fout (params_.io_path_.str().cpp_str());
  nih::json::Json::dump(handle, &fout);
  fout.close();
}

void Session::load() {
  if (!params_.io_path_.exists()) {
    LOG(FATAL) << "File: " << params_.io_path_.str() << " not existes";
  }
  std::ifstream fin (params_.io_path_.str().cpp_str());
  nih::json::Json handle { nih::json::Json::load(&fin) };
  for (auto& o : ops_) {
    o->load(handle);
  }
  fin.close();
}

std::vector<std::unique_ptr<Operation>> Operation::create(Parameter params) {
  std::vector<std::unique_ptr<pipeline::Operation>> ops;
  if (params.is_predict_) {
    std::unique_ptr<Operation> predict_ops(new PredictOp(params));
    ops.push_back(std::move(predict_ops));
  } else {
    std::unique_ptr<pipeline::Operation> train_op(new pipeline::TrainOp(params));
    ops.push_back(std::move(train_op));
  }
  return ops;
}

}  // namespace pipeline
}  // namespace mimic