/*!
 *  Copyright (c) 2018 by XGBoost Contributors
 * \file filesystem.h
 * \brief Utilities to manipulate files
 * \author Hyunsu Philip Cho
 */
// LICENSE apache 2.0

#include <string>
#include <nih/logging.hh>

#include "mimic/basic/FileSystem.hh"


namespace mimic {
Directory::Directory(std::string dirname) {
  dir_ = opendir(dirname.c_str());
  if (!dir_) { LOG(FATAL) << "Error opendir()"; }
}

TemporaryDirectory::TemporaryDirectory(bool verbose)
    : verbose_{verbose} {
  std::string tmproot; /* root directory of temporary area */
  std::string dirtemplate; /* template for temporary directory name */
  /* Get TMPDIR env variable or fall back to /tmp/ */
  {
    const char* tmpenv = getenv("TMPDIR");
    if (tmpenv) {
      tmproot = std::string(tmpenv);
      // strip trailing forward slashes
      while (tmproot.length() != 0 && tmproot[tmproot.length() - 1] == '/') {
        tmproot.resize(tmproot.length() - 1);
      }
    } else {
      tmproot = "/tmp";
    }
  }
  dirtemplate = tmproot + "/tmpdir.XXXXXX";
  std::vector<char> dirtemplate_buf(dirtemplate.begin(), dirtemplate.end());
  dirtemplate_buf.push_back('\0');
  char* tmpdir = mkdtemp(&dirtemplate_buf[0]);
  if (!tmpdir) {
    LOG(FATAL) << "TemporaryDirectory(): "
               << "Could not create temporary directory";
  }
  path_ = std::string(tmpdir);
}

void TemporaryDirectory::RemoveDirectoryRecursively(std::string const& dirname) {
  Directory dir = Directory(dirname);
  struct dirent *entry;
  char path[PATH_MAX];

  while ((entry = readdir(dir.dir_)) != NULL) {
    if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
      snprintf(path, (size_t) PATH_MAX, "%s/%s",
               dirname.c_str(), entry->d_name);
      if (entry->d_type == DT_DIR) {
        RemoveDirectoryRecursively(path);
      }

      if (std::remove(path) != 0) {
        LOG(FATAL) << "Removing " << path << " failed.";
      }
    }
  }
  rmdir(dirname.c_str());
}
}  // namespace mimic