#include <string>
#include <utility>

#include <nih/logging.hh>
#include "mimic/basic/monitor.hh"

namespace mimic {

Timer::Timer(std::string _label) : label_(std::move(_label)) {
    start_ = Clock::now();
#if defined(ENABLE_PROFILING) && ENABLE_PROFILING == 1
    LOG(INFO) << label_ << " start.";
#endif
  }

Timer::~Timer() {
  elapsed_ = Clock::now() - start_;
#if defined(ENABLE_PROFILING) && ENABLE_PROFILING == 1
  LOG(INFO) << label_ << " elapsed: " << Seconds(elapsed_).count();
#endif
}

}  // namespace mimic