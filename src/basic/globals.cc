#include <string>
#include <nih/logging.hh>

#include "mimic/kiwi.h"

KiwiAPI
Kiwi_ErrorCode Kiwi_globalSetParam(char const* _key, char const* _value) {
  std::string key {_key}, value {_value};
  if (key == "verbosity") {
    nih::Log::setGlobalVerbosity(value);
  } else {
    LOG(WARNING) << "Unknown parameter: " << key;
  }
  return static_cast<int>(kSuccess);
}