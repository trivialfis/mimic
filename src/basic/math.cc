#include "mimic/basic/math.hh"
#include "mimic/basic/eigen_types.hh"

namespace mimic {
namespace math {

Eigen::VectorXf logSumExp(MatrixXfC const& value, McInt axis) {
  ASSERT(axis == 0 || axis == 1);

  Eigen::VectorXf a_max = amax(value, axis);
  for (auto ind : Range(a_max.size())) {
    if (std::isinf(a_max(ind)) || std::isnan(a_max(ind))) {
      a_max(ind) = 0;
    }
  }

  MatrixXfC normalised( value.rows(), value.cols() );
  if (axis == 0) {
    for (auto c : Range(value.cols())) {
      normalised.col(c).array() =
          (value.col(c).array() - a_max(c)).array().exp();
    }
  } else {
    for (auto r : Range(value.rows())) {
      normalised.row(r).array() =
          (value.row(r).array() - a_max(r)).array().exp();
    }
  }

  Eigen::VectorXf sum;
  if (axis == 0) {
    sum.resize(normalised.cols());
    for (auto r : Range(normalised.cols())) {
      sum(r) = normalised.col(r).sum();
    }
  } else {
    sum.resize(normalised.rows());
    for (auto c : Range(normalised.rows())) {
      sum(c) = normalised.row(c).sum();
    }
  }

  Eigen::VectorXf res = sum.array().log();
  res.array() += a_max.array();
  return res;
}

}  // namespace math
}  // namespace mimic