#include <random>
#include "mimic/basic/random.hh"

namespace mimic {

int RandomDevice::seed_ = 0;
bool RandomDevice::is_seeded_ = false;

int RandomDevice::seed(int s) {
  int old = seed_;
  seed_ = s;
  is_seeded_ = true;
  return old;
}

int RandomDevice::reset() {
  is_seeded_ = false;
  return seed_;
}

RandomDevice::ResultType RandomDevice::gen() {
  if (is_seeded_) {
    return seed_;
  }
  static std::random_device dev;
  return dev();
}

}  // namespace mimic