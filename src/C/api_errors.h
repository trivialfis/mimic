#ifndef C_API_ERRORS_H_
#define C_API_ERRORS_H_

#define ISSUE_URL "Please open a bug report in https://gitlab.com/"

#define C_API_BEG() try {


#define C_API_END()                                             \
  } catch (nih::NIHError const& e) {                            \
    std::cerr << e.what();                                      \
    return static_cast<Kiwi_ErrorCode>(kFatal);                 \
  } catch (std::exception const& e) {                           \
    LOG(FATAL) << "Internal Error. "  << e.what() << ISSUE_URL; \
  }                                                             \
  return static_cast<Kiwi_ErrorCode>(kSuccess);

#endif  // C_API_ERRORS_H_