#include <vector>
#include <string>
#include <memory>
#include <map>

#include <nih/strings.hh>
#include <nih/logging.hh>

#include "mimic/basic/range.hh"
#include "mimic/models/model.hh"
#include "mimic/pipeline/session.hh"

class CLIArgumentsManager {
  std::vector<std::string> arguments_;
  mimic::pipeline::Parameter param_;
  std::map<std::string, std::string> arguments_map_;

 public:
  // FIXME(trivialfis): Introduce a graph to represent params.
  CLIArgumentsManager(int argc, char *const argv[]) {
    if (argc == 1) {
      LOG(USER_E) << "Please supply an argument.";
    }
    for (size_t i = 1; i < argc; ++i) {
      arguments_.push_back(argv[i]);
    }
    for (auto const& arg : arguments_) {
      std::vector<std::string> splited;
      mimic::Split(splited, arg, [](char c) -> bool { return c == '='; });
      if (splited.size() != 2) {
        LOG(USER_E) << "Wrong argument format: " << arg;
      }
      std::string parameter {std::move(splited[0])};
      std::string argument {std::move(splited[1])};
      if (parameter == "--input-file") {
        param_.input_filename = argument;
      } else if (parameter == "--load") {
        param_.is_predict_ = true;
        param_.io_path_ = argument;
      } else if (parameter == "--model") {

        if (argument == "baseline") {
          param_.model = mimic::model::Model::kBaseline;
        } else if (argument == "dist-hmm") {
          param_.model = mimic::model::Model::kDistHMM;
        } else if (argument == "hmm") {
          param_.model = mimic::model::Model::kHMM;
        } else if (argument == "vbhmm") {
          param_.model = mimic::model::Model::kVBHMM;
        } else if (argument == "full-vbhmm") {
          param_.model = mimic::model::Model::kFullVBHMM;
        } else {
          param_.model = mimic::model::Model::kUnknown;
        }

      } else if (parameter == "--iterations") {
        param_.iterations = mimic::ston<mimic::McInt>(argument);
      } else if (parameter == "--seed") {
        param_.seed = mimic::ston<mimic::McInt>(argument);
      } else if (parameter == "--abort") {
        param_.abort = static_cast<bool>(mimic::ston<mimic::McInt>(argument));
      } else if (parameter == "--rate") {
        param_.learning_rate_ = mimic::ston<mimic::McFloat>(argument);
      } else if (parameter == "--dump") {
        param_.io_path_ = argument;
      }

      else if (parameter == "--vbhmm:subchain") {
        param_.vbhmm_subchain_len_ = mimic::ston<mimic::McInt>(argument);
      } else if (parameter == "--vbhmm:buffer") {
        param_.vbhmm_buffer_len_ = mimic::ston<mimic::McInt>(argument);
      } else if (parameter == "--vbhmm:states") {
        param_.vbhmm_num_states_ = mimic::ston<mimic::McInt>(argument);
      }

      else {
        LOG(USER_E) << "Unknow parameter: " << arg;
      }
    }
  }
  ~CLIArgumentsManager() = default;

  mimic::pipeline::Parameter GetParameter() const {
    return param_;
  }
};

int main(int argc, char *const argv[]) {
  using namespace mimic;
  pipeline::Parameter param;
  pipeline::Session main_session;

  try {
    nih::Log::setGlobalVerbosity(nih::Log::ErrorType::kInfo);
    auto manager = CLIArgumentsManager(argc, argv);
    param = manager.GetParameter();
    auto ops = pipeline::Operation::create(param);
    main_session.take(ops);
    main_session.setParameters(param);
  } catch (nih::NIHError& error) {
    std::cout << error.what() << std::endl;
    return 1;
  }

  if (param.abort) {
    main_session.run();
  } else {
    try {
      main_session.run();
    } catch (nih::NIHError& error) {
      std::cout << error.what() << std::endl;
      return 1;
    }
  }

  return 0;
}