#include <Eigen/Dense>

#include <cmath>
#include <random>
#include <numeric>

#include <nih/logging.hh>

#include "../basic/basic.h"
#include "mimic/basic/monitor.hh"

#include "mimic/models/model.hh"
#include "mimic/models/hmm.hh"
#include "mimic/C/models.h"

namespace mimic {
namespace model {

HiddenMarkovModel::HiddenMarkovModel()
    : Model(kHMM),
      initialized_(false) {}

HiddenMarkovModel::Parameter::Parameter() :
    n_states(0),
    n_symbols(0) {}

HiddenMarkovModel::Parameter::Parameter(
    McInt _n_states, McInt _n_symbols) :
    n_states(_n_states),
    n_symbols(_n_symbols) {}

void HiddenMarkovModel::Parameter::SetNumStates(McInt val) {
  n_states = val;
}
void HiddenMarkovModel::Parameter::SetNumSymbols(McInt val) {
  n_symbols = val;
}

HiddenMarkovModel::HiddenMarkovModel(Parameter _param) :
    HiddenMarkovModel(_param.n_states, _param.n_symbols) {}

HiddenMarkovModel::HiddenMarkovModel(McInt n_states, McInt n_symbols)
    : Model(kHMM), param_ {n_states, n_symbols},
      transition_(n_states, n_states),
      emission_(n_states, n_symbols),
      initial_(n_states), initialized_(false) {
  // std::random_device rd;
  // std::mt19937 gen(rd());
  // std::uniform_real_distribution<> dist(0.0f, 1.0f);
  for (McInt i = 0; i < n_states; ++i) {
    initial_[i] = 1.0f / n_states;
  }
  for (McInt i = 0; i < n_states; ++i) {
    for (McInt j = 0; j < n_states; ++j) {
      transition_(i, j) = 1.0f / n_states;
    }
  }
  for (McInt i = 0; i < n_symbols; ++i) {
    for (McInt j = 0; j < n_states; ++j) {
      emission_(j, i) = 1.0f / n_symbols;
    }
  }
  this->finalize();
  // McFloat sum = initial_.sum();
  // initial_ /= sum;
}

MatrixXfC HiddenMarkovModel::forward(Eigen::VectorXf const& _sequence,
                                     Eigen::VectorXf&  _scales) const {
  Timer timer("Forward");
  McInt time = _sequence.rows();
  MatrixXfC seq_alphas(initial_.size(), _sequence.size());
  _scales = MatrixXfC::Zero(time, 1);

  Eigen::VectorXf alphas(initial_);
  McFloat symbol = _sequence(0);
  // Initialization step.
  {
    Eigen::VectorXf const& emmisions_to_symbol = emission_.col(symbol);
    alphas = alphas.array() * emmisions_to_symbol.array();
    _scales[0] = alphas.sum();
    _scales[0] = 1 / _scales[0];
    alphas.array() *= _scales[0];
    seq_alphas.col(0) = alphas;
  }

  for (McInt t = 1; t < time; ++t) {
    _scales[t] = 0;
    Eigen::VectorXf const A = transition_ * seq_alphas.col(t-1);
    symbol = _sequence(t);
    ASSERT(symbol < emission_.cols())
        << " Invalid symbol encountered: " << symbol << ", "
        << " n symbols: " << emission_.cols();
    Eigen::VectorXf const& emissions_to_symbol = emission_.col(symbol);
    seq_alphas.col(t) = A.array() * emissions_to_symbol.array();
    _scales[t] = 1.0f / seq_alphas.col(t).sum();
    seq_alphas.col(t) *= _scales[t];
  }
  return seq_alphas;
}

MatrixXfC HiddenMarkovModel::forward(Eigen::VectorXf const& sequence) {
  MatrixXfC alphas = this->forward(sequence, scales_);
  initialized_ = true;
  return alphas;
}

MatrixXfC HiddenMarkovModel::backward(Eigen::VectorXf const &sequence) const {
  Timer timer("Backward");
  McInt time = sequence.size();
  McInt n_states = transition_.rows();
  MatrixXfC seq_betas (n_states, time);

  Eigen::VectorXf betas = Eigen::VectorXf::Constant(n_states, 0.0f);
  seq_betas.col(time-1).array() = scales_[time-1];

  for (McInt t = time - 2; t >= 0; --t) {
    McFloat symbol = sequence(t+1);
    Eigen::VectorXf const& emission = emission_.col(symbol);
    for (McInt state = 0; state < n_states; ++state) {
      Eigen::VectorXf const& transition = transition_.row(state);
      Eigen::VectorXf const& beta = transition.array() * emission.array();
      betas(state) = std::move(beta.dot(seq_betas.col(t+1)));
    }
    seq_betas.col(t) = betas.array() * scales_(t);
  }
  return seq_betas;
}

std::pair<MatrixXfC, std::vector<MatrixXfC>> HiddenMarkovModel::estimateStates(
    Eigen::VectorXf const& sequence) {
  McInt time = sequence.size();
  McInt n_states = transition_.rows();

  MatrixXfC gammas =
      MatrixXfC::Zero(transition_.rows(), sequence.size());
  std::vector<MatrixXfC> digammas (time,
                                   MatrixXfC::Zero(n_states, n_states));

  MatrixXfC const& forward_mat = forward(sequence);
  MatrixXfC const& backward_mat = backward(sequence);

  Timer timer("EstimateStates");
  for (McInt t = 0; t < time-1; ++t) {
    McFloat symbol = sequence(t+1);
    auto& t_digammas = digammas.at(t);
    Eigen::VectorXf const& f_c = forward_mat.col(t);
    auto const& eb_c =
        emission_.col(symbol).array() * backward_mat.col(t+1).array();
    for (McInt i = 0; i < n_states; ++i) {
      t_digammas.row(i) =
          f_c(i) * transition_.row(i).transpose().array() * eb_c;
      gammas(i, t) += t_digammas.row(i).sum();
    }
  }
  gammas.col(time-1) = forward_mat.col(time-1);

  return std::make_pair(gammas, digammas);
}

void HiddenMarkovModel::reestimate(const Eigen::VectorXf &sequence) {
  // Re-estimate
  MatrixXfR digammas_sum {
    MatrixXfR::Constant(transition_.rows(), transition_.cols(), 0)};
  std::pair<MatrixXfC, std::vector<MatrixXfC>> gamma_pair =
      estimateStates(sequence);

  Timer timer("ReEstimate");
  MatrixXfC& gammas = gamma_pair.first;
  auto& digammas = gamma_pair.second;
  Eigen::VectorXf gammas_sum { Eigen::VectorXf::Constant(gammas.rows(), 0) };

  for (McInt t = 0; t < sequence.size()-1; ++t) {
    digammas_sum.array() += digammas.at(t).array();
    gammas_sum.array() += gammas.col(t).array();
  }
  // re-estimate transiton
  for (McInt s = 0; s < transition_.rows(); ++s) {
    transition_.row(s).array() =
        digammas_sum.row(s).array() / gammas_sum(s);
  }
  // re-estimate emission
  for (McInt i = 0; i < emission_.rows(); ++i) {
    McFloat denom = 0;
#pragma omp parallel for reduction(+: denom) schedule(static)
    for (McInt t = 0; t < sequence.size(); ++t) {
      denom += gammas(i, t);
    }
    for (McInt j = 0; j < emission_.cols(); ++j) {
      McFloat numer = 0;
#pragma omp parallel for reduction(+: numer) schedule(static)
      for (McInt t = 0; t < sequence.size(); ++t) {
        if (sequence(t) == j) {
          numer += gammas(i, t);
        }
      }
      emission_(i, j) = numer / denom;
    }
  }
}

void HiddenMarkovModel::finalize() {
  for (McInt r = 0; r < transition_.rows(); ++r) {
    transition_.row(r).array() /= transition_.row(r).sum();
    emission_.row(r).array() /= emission_.row(r).sum();
  }
  initial_.array() /= initial_.sum();
}

void HiddenMarkovModel::step(MatrixXfR const& obs) {
  Timer timer("Train");
  NIH_ASSERT_EQ(obs.rows(), 1)
      << "Multiple observation sequences is not supported.";

  Eigen::VectorXf const& sequence = obs.row(0);
  auto count_uniques
      = [](Eigen::VectorXf const& sequence) {
          auto maximum =
              std::accumulate(sequence.data(), sequence.data() + sequence.size(),
                              (std::numeric_limits<McFloat>::min)(),
                              [](McFloat lhs, McFloat rhs) {
                                return std::max(lhs, rhs);
                              });
          return maximum + 1;
        };

  if (emission_.cols() == 0) {
    McInt n_symbols = count_uniques(sequence);
    param_.SetNumSymbols(n_symbols);
    emission_ = MatrixXfR::Constant(
        param_.n_states, n_symbols, 1.0f / param_.n_states);
  }
  this->reestimate(sequence);
}

McFloat HiddenMarkovModel::measure(MatrixXfR const& sequence) const {
  Eigen::VectorXf scales_buffer = MatrixXfC::Zero(sequence.size(), 1);
  Eigen::VectorXf const *scales;
  if (!initialized_) {
    LOG(WARNING) << "Not initialized.";
    this->forward(sequence.row(0), scales_buffer);
    scales = &scales_buffer;
  } else {
    scales = &scales_;
  }
  std::transform(scales->data(), scales->data() + scales->size(),
                 scales_buffer.data(),
                 [](McFloat x) {
                   return std::log(x);
                 });
  McFloat logprob =
      std::accumulate(scales_buffer.data(), scales_buffer.data() + scales_buffer.size(),
                      0.0f);
  return -logprob;
}

}  // namespace model
}  // namespace mimic

KiwiAPI Kiwi_model Kiwi_HmmInit(int nstates, int nsymbols) {
  mimic::model::Model* model = nullptr;
  model = new mimic::model::HiddenMarkovModel(nstates, nsymbols);
  ASSERT(model) << "Failed to create Hidden Markov model.";
  return reinterpret_cast<Kiwi_model>(model);
}