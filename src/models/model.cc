#include <string>

#include <nih/logging.hh>
#include "mimic/models/model.hh"
#include "mimic/models/vbhmm.hh"
#include "mimic/models/hmm.hh"
#include "mimic/models/hmm_states.hh"
#include "mimic/kiwi.h"
#include "mimic/models/baseline.hh"

#include "mimic/C/models.h"
#include "../C/api_errors.h"

namespace mimic {
namespace model {

Model* Model::create(std::string name) {
  ModelKind kind = toKind(name);
  return create(kind);
}

Model* Model::create(ModelKind kind) {
  Model* ptr = nullptr;

  switch (kind) {
    case kUnknown:   LOG(FATAL) << "Unknown model.";
    case kBaseline:  ptr = new BaseLineDetector();
    case kHMM:       ptr = new HiddenMarkovModel();
    case kDistHMM:   ptr = new DistHiddenMarkovModel();
    case kVBHMM:     ptr = new VBHMM();
    case kFullVBHMM: ptr = new VBHMM();  // FIXME
  }
  return ptr;
}

std::string Model::toName(Model::ModelKind kind) {
  switch (kind) {
    case kUnknown:   return "Unknown";
    case kBaseline:  return "Baseline";
    case kHMM:       return "HMM";
    case kDistHMM:   return "DistHMM";
    case kVBHMM:     return "VBHMM";
    case kFullVBHMM: return "FullVBHMM";
    default:
      LOG(FATAL) << "Unknown kind";
      return "";
  }
}

Model::ModelKind Model::toKind(std::string name) {
  if ( name == "VBHMM" ) {
    return kVBHMM;
  } else if ( name == "HMM" ) {
    return kHMM;
  } else if ( name == "DistHMM" ) {
    return kDistHMM;
  } else if ( name == "Baseline" ) {
    return kBaseline;
  } else {
    return kUnknown;
  }
}

Model::ModelKind Model::getKind() const {
  return kind_;
}

std::string Model::getName() const {
  auto name = toName(kind_);
  return name;
}

}  // namespace model
}  // namespace mimic

KiwiAPI
Kiwi_model Kiwi_createModel(char const* name) {
  try {
    mimic::model::Model* model = mimic::model::Model::create(name);
    return reinterpret_cast<Kiwi_model>(model);
  } catch (nih::NIHError const& e) {
    std::cerr << e.what();
  } catch (std::exception& e) {
    LOG(FATAL) << "Internal Error. "  << e.what() << ISSUE_URL;
  }
  return nullptr;
}

KiwiAPI
Kiwi_ErrorCode Kiwi_destroyModel(Kiwi_model _model) {
  C_API_BEG()
  {
    mimic::model::Model* m = reinterpret_cast<mimic::model::Model*>(_model);
    LOG_VAR(m);
    if (m) {
      delete m;
    } else {
      LOG(FATAL) << "Destroying an empty model.";
    }
  }
  C_API_END()
}

#define CHECK_EMPTY_MODEL(p_handle)                                     \
  mimic::model::Model* model_casted =                                   \
                 reinterpret_cast<mimic::model::Model*>(p_handle);      \
  if (!model_casted) {                                                  \
    LOG(FATAL) << "Empty model.";                                       \
  }                                                                     \
  mimic::model::Model& model = *model_casted;

#define CHECK_EMPTY_SEQ(p_seq)                                          \
  mimic::MatrixXfR* const obs_casted =                                  \
                  reinterpret_cast<mimic::MatrixXfR *const>(p_seq);     \
  if (!obs_casted) {                                                    \
    LOG(FATAL) << "Empty sequence.";                                    \
  }                                                                     \
  mimic::MatrixXfR const& obs = *obs_casted;

KiwiAPI
Kiwi_ErrorCode Kiwi_modelStep(Kiwi_model p_model, Kiwi_sequence p_seq) {
  C_API_BEG()
  {
    CHECK_EMPTY_MODEL(p_model);
    CHECK_EMPTY_SEQ(p_seq);
    model.step(obs);
  }
  C_API_END()
}

KiwiAPI
Kiwi_ErrorCode Kiwi_modelMeasure(
    Kiwi_model handle, Kiwi_sequence seq, mimic::McFloat* score) {
  C_API_BEG()
  {
    CHECK_EMPTY_MODEL(handle);
    CHECK_EMPTY_SEQ(seq);
    mimic::McFloat res = model.measure(obs);
    *score = res;
  }
  C_API_END()
}

KiwiAPI
Kiwi_ErrorCode Kiwi_modelSave(Kiwi_model p_model, char* cpath) {
  C_API_BEG();
  {
    CHECK_EMPTY_MODEL(p_model);
    std::string path {cpath};
    using namespace nih::json;
    Json handle { Object() };
    model.save(handle);
    std::ofstream fout (path);
    if (fout) {
      Json::dump(handle, &fout);
    } else {
      LOG(FATAL) << "Failed to open file: " << path;
    }
  }
  C_API_END();
}

KiwiAPI
Kiwi_ErrorCode Kiwi_modelLoad(Kiwi_model p_model, char* cpath) {
  C_API_BEG();
  {
    CHECK_EMPTY_MODEL(p_model);
    std::string path {cpath};
    using namespace nih::json;
    Json handle { Object() };
    std::ifstream fin (path);
    if (fin) {
      handle = Json::load(&fin);
    } else {
      LOG(FATAL) << "Failed to open file: " << path;
    }
    model.load(handle);
  }
  C_API_END();
}