#include <numeric>
#include <utility>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <nih/logging.hh>

#include "mimic/basic/eigen_types.hh"
#include "mimic/basic/math.hh"
#include "mimic/basic/rtti.hh"
#include "mimic/basic/range.hh"

#include "mimic/models/hmm_states.hh"

namespace mimic {
namespace model {

DistHiddenMarkovModel::DistHiddenMarkovModel() : Model(kDistHMM) {}

DistHiddenMarkovModel::DistHiddenMarkovModel(
    McInt k_states, MatrixXfC transitions,
    Eigen::VectorXf pi,
    std::vector<std::shared_ptr<dist::Distribution>> emits,
    MatrixXfR const& obs) :

    Model(kDistHMM),
    k_states_(k_states),
    transitions_(transitions),
    pi_(pi),
    emits_(std::move(emits))
{
  xi_ = Eigen::Tensor<McFloat, 4>(obs.rows(), obs.cols()-1, k_states_, k_states_);
  xi_.setZero();
  gammas_ = Eigen::Tensor<McFloat, 3>(obs.rows(), obs.cols(), k_states);
  gammas_.setZero();
  eStep(obs);
}

void DistHiddenMarkovModel::eStep(MatrixXfR const& obs) {
  for (auto r : Range(0, obs.rows())) {
    eStepRow(r, obs);
  }
}

void DistHiddenMarkovModel::eStepRow(McInt row_id, MatrixXfR const& obs) {
  this->eStepRowSubchain(row_id, 0, obs.row(row_id).cols(), 0, obs);
}

void DistHiddenMarkovModel::eStepRowSubchain(
    McInt row_id, McInt start, McInt end, McInt buf_size, MatrixXfR const& obs) {
  McInt const length = end - start + buf_size * 2;
  // Copy subchain
  Eigen::VectorXf x (length);
  McInt k = 0;
  for (auto i : Range(start - buf_size, end + buf_size)) {
    x(k) = obs(row_id, i);
    k++;
  }

  Eigen::VectorXf x_t (1);
  MatrixXfC log_emits(k_states_, length);
  for (auto r : Range(0, length)) {
    for (auto k : Range(k_states_)) {
      x_t[0] = x(r);
      log_emits(k, r) = emits_.at(k)->logProbability(x_t);
    }
  }

  MatrixXfC const& alpha = forward(x, log_emits.transpose()).first;
  MatrixXfC const& beta = backward(x, log_emits.transpose());

  // gamma updates
  for (auto t : Range(buf_size, x.size() - buf_size)) {
    auto res =
        math::norm(alpha.col(t).array() + beta.col(t).array()).first;
    for (auto k : Range(res.size())) {
      gammas_(row_id, t + start - buf_size, k) = res(k);
    }
  }

  // xi updates
  for (auto t : Range(buf_size, x.size() - buf_size - 1)) {
    // FIXME: Is column right?
    auto const& psi_mul_beta = log_emits.col(t+1) + beta.col(t+1);
    MatrixXfC const& outer =
        math::louter(alpha.col(t), psi_mul_beta).transpose();
    auto const& trans_mul_outer = transitions_ + outer;
    MatrixXfC const& res = math::norm(trans_mul_outer).first;
    for (auto i : Range(k_states_)) {
      for (auto j : Range(k_states_)) {
        xi_(row_id, t + start - buf_size, i, j) = res(i, j);
      }
    }
  }
}

MatrixXfC DistHiddenMarkovModel::calcLocalTransition(McInt a, McInt b) const {
  MatrixXfC res = MatrixXfC::Zero(k_states_, k_states_);
  Eigen::VectorXf x (b - a);
  for (auto i : Range(k_states_)) {
    for (auto j : Range(k_states_)) {
      for (auto t : Range(a, b)) {
        x(t-a) = xi_(0, t, i, j);
      }
      // transition from s_i to s_j
      res(i, j) = math::logSumExp(x.data(), x.data() + x.size());
    }
  }
  return res;
}

MatrixXfC DistHiddenMarkovModel::calcTransition() const {
  McInt obs_length = xi_.dimension(1);
  return calcLocalTransition(0, obs_length);
}

Eigen::VectorXf DistHiddenMarkovModel::calcStart() const {
  Eigen::VectorXf sum(gammas_.dimension(0));
  Eigen::VectorXf res(k_states_);
  for (auto k : Range(0, k_states_)) {
    for (auto n : Range(0, gammas_.dimension(0))) {  // iterate through sequences
      sum(n) = gammas_(n, 0, k);
    }
    res(k) = math::logSumExp(sum.data(), sum.data() + sum.size());
  }
  return res;
}

McFloat DistHiddenMarkovModel::logLikelihood(MatrixXfR const& obs) const {
  McFloat res { 0 };
  MatrixXfC psi { MatrixXfC::Zero(obs.cols(), k_states_) };
  for (auto r : Range(0, obs.rows())) {
    auto const& x = obs.row(r);
    for (auto t : Range(x.size())) {
      for (auto k : Range(emits_.size())) {
        Eigen::VectorXf xv(1);
        xv << x[t];
        psi(t, k) = emits_.at(k)->logProbability(xv);
      }
    }
    Eigen::VectorXf z = forward(x, psi).second;
    res += z.sum();
  }
  return res;
}

std::pair<MatrixXfC, Eigen::VectorXf> DistHiddenMarkovModel::forward(
    Eigen::VectorXf const& obs, MatrixXfC const& log_emit) const {
  auto norm = math::norm(log_emit.row(0).transpose().array() + pi_.array());
  MatrixXfC alphas (log_emit.cols(), obs.size());
  alphas.col(0) = norm.first.array();
  Eigen::VectorXf zs (obs.size());
  zs(0) = norm.second;
  for (auto t : Range(1, obs.size())) {
    Eigen::VectorXf mult =
        math::ldot(transitions_.transpose(), alphas.col(t-1));
    auto temp = math::norm(log_emit.row(t).transpose().array() + mult.array());
    alphas.col(t) = temp.first;
    zs(t) = temp.second;
  }
  return std::make_pair(std::move(alphas), std::move(zs));
}

MatrixXfC DistHiddenMarkovModel::backward(
    Eigen::VectorXf obs, MatrixXfC const& log_emit) {
  MatrixXfC betas = MatrixXfC::Zero(log_emit.cols(), obs.size());
  betas.col(obs.size() - 1) = Eigen::VectorXf::Zero(k_states_);
  for (auto t : Range(obs.size() - 2, -1, -1)) {
    MatrixXfC mult =
        log_emit.row(t+1).transpose().array() + betas.col(t+1).array();
    auto res = math::ldot(transitions_, mult);
    betas.col(t) = res;
  }
  return betas;
}

void DistHiddenMarkovModel::updateStart() {
  pi_ = calcStart().array() - std::log(gammas_.dimension(0));
}

void DistHiddenMarkovModel::updateTransition() {
  MatrixXfC const& expectation = calcTransition();
  for (auto k : Range(0, k_states_)) {
    transitions_.row(k) = math::norm(expectation.row(k)).first;
  }
}

void DistHiddenMarkovModel::mStep(const MatrixXfR &obs) {
  updateTransition();
  updateStart();

  Eigen::array<McInt, 3> offsets = {0, 0, 0};
  Eigen::array<McInt, 3> extents = {1, obs.cols(), 1};
  Eigen::array<McInt, 1> const dims {{obs.cols()}};
  for (auto k : Range(0, k_states_)) {
    auto p = emits_.at(k).get();
    offsets[2] = k;
    Eigen::Tensor<McFloat, 1> const& slice =
        gammas_.slice(offsets, extents).reshape(dims);
    Eigen::VectorXf gammas_at_state_k(gammas_.dimension(1));
    for (auto ind : Range(gammas_.dimension(1))) {
      gammas_at_state_k(ind) = slice(ind);
    }
    auto suff = cast<dist::Exponential>(p)->calcExpectedSufficient(
        obs, gammas_at_state_k);
    cast<dist::Exponential>(p)->setNatural(suff);
  }
}

void DistHiddenMarkovModel::step(MatrixXfR const& obs) {
  eStep(obs);
  mStep(obs);
}

McFloat DistHiddenMarkovModel::measure(const MatrixXfR &obs) const {
  return this->logLikelihood(obs);
}

void DistHiddenMarkovModel::save(nih::json::Json &handle) const {
  namespace js = nih::json;
  handle["k_states"] = js::Number(k_states_);

  // transitions
  std::vector<js::Json> transitions_shape (2);
  transitions_shape[0] = js::Number(transitions_.rows());
  transitions_shape[1] = js::Number(transitions_.cols());
  handle["transitions_shape"] = js::Array(transitions_shape);
  std::vector<js::Json> transitions_json(transitions_.size());
  for (auto i : Range(transitions_.size())) {
    transitions_json[i] = js::Number(transitions_.data()[i]);
  }
  handle["transitions"] = js::Array(std::move(transitions_json));

  // pi
  std::vector<js::Json> pi_json (pi_.size());
  for (auto i : Range(pi_.size())) {
    pi_json[i] = js::Number(pi_[i]);
  }
  handle["pi"] = js::Array(std::move(pi_json));

  // emits
  std::vector<js::Json> emissions_json (emits_.size());
  for (auto i : Range(emits_.size())) {
    emissions_json[i] = js::Object();
    emissions_json[i]["name"] = js::String(emits_[i]->getKindStr());
    emissions_json[i]["dist"] = js::Object();
    emits_[i]->save(emissions_json[i]["dist"]);
  }
  handle["emissions"] = js::Array(std::move(emissions_json));

  // gammas
  std::vector<js::Json> gammas_shape_json(gammas_.rank());
  for (auto i : Range(gammas_shape_json.size())) {
    gammas_shape_json[i] = js::Number(gammas_.dimension(i));
  }
  handle["gammas_shape"] = js::Array(gammas_shape_json);
  std::vector<js::Json> gammas_json (gammas_.size());
  for (auto i : Range(gammas_.size())) {
    gammas_json[i] = js::Number(gammas_.data()[i]);
  }
  handle["gammas"] = js::Array(gammas_json);

  // xi
  std::vector<js::Json> xi_shape_json(xi_.rank());
  for (auto i : Range(xi_shape_json.size())) {
    xi_shape_json[i] = js::Number(xi_.dimension(i));
  }
  handle["xi_shape"] = js::Array(xi_shape_json);
  std::vector<js::Json> xi_json (xi_.size());
  for (auto i : Range(xi_.size())) {
    xi_json[i] = js::Number(xi_.data()[i]);
  }
  handle["xi"] = js::Array(xi_json);
}

void DistHiddenMarkovModel::load(nih::json::Json const& handle) {
  namespace js = nih::json;
  k_states_ = js::get<js::Number>(handle["k_states"]);

  // transitions
  std::vector<js::Json> transitions_shape_json =
      js::get<js::Array>(handle["transitions_shape"]);
  transitions_.resize(
      js::get<js::Number>(transitions_shape_json[0]),
      js::get<js::Number>(transitions_shape_json[1]));
  std::vector<nih::json::Json> const& transitions_json =
      js::get<js::Array>(handle["transitions"]);
  for (auto i : Range(transitions_.size())) {
    transitions_.data()[i] = js::get<js::Number const>(transitions_json[i]);
  }

  // pi
  std::vector<js::Json> pi_json = js::get<js::Array>(handle["pi"]);
  pi_.resize(pi_json.size());
  for (auto i : Range(pi_json.size())) {
    pi_[i] = js::get<js::Number>(pi_json[i]);
  }

  // emits
  std::vector<js::Json> emissions_json =
      js::get<js::Array>(handle["emissions"]);
  emits_.resize(emissions_json.size());
  for (auto i : Range(emits_.size())) {
    std::string name = js::get<js::String>(emissions_json[i]["name"]);
    emits_[i].reset(dist::Distribution::create(name));
    emits_[i]->load(emissions_json[i]["dist"]);
  }

  // gammas
  std::vector<js::Json> gammas_shape_json =
      js::get<js::Array>(handle["gammas_shape"]);
  gammas_.resize(js::get<js::Number>(gammas_shape_json[0]),
                 js::get<js::Number>(gammas_shape_json[1]),
                 js::get<js::Number>(gammas_shape_json[2]));
  std::vector<js::Json> gammas_json = js::get<js::Array>(handle["gammas"]);
  for (auto i : Range(gammas_.size())) {
    gammas_.data()[i] = js::get<js::Number>(gammas_json[i]);
  }

  // xi
  std::vector<nih::json::Json> xi_shape_json =
      js::get<js::Array>(handle["xi_shape"]);
  xi_.resize(js::get<js::Number>(xi_shape_json[0]),
             js::get<js::Number>(xi_shape_json[1]),
             js::get<js::Number>(xi_shape_json[2]),
             js::get<js::Number>(xi_shape_json[3]));
  std::vector<js::Json> xi_json = js::get<js::Array>(handle["xi"]);
  for (auto i : Range(xi_.size())) {
    xi_.data()[i] = js::get<js::Number>(xi_json[i]);
  }
}

}  // namespace model
}  // namespace mimic