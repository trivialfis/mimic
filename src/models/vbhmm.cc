#include <random>
#include <utility>

#include <boost/math/special_functions/digamma.hpp>
#include <Eigen/Dense>

#include "nih/json.hh"

#include "mimic/C/models.h"

#include "mimic/basic/eigen_types.hh"
#include "mimic/basic/math.hh"
#include "mimic/basic/monitor.hh"
#include "mimic/basic/range.hh"
#include "mimic/basic/random.hh"
#include "mimic/basic/rtti.hh"

#include "mimic/distributions/multinoulli.hh"

#include "mimic/models/vbhmm.hh"

namespace mimic {
namespace model {

VBHMM::VBHMM() : Model(kVBHMM), initialized(false) {}

VBHMM::VBHMM(McInt n_states,
             std::shared_ptr<dist::Dirichlet> _u_a,
             std::shared_ptr<dist::Dirichlet> _u_pi,
             std::vector<std::shared_ptr<dist::Dirichlet>> _u_d,
             std::vector<std::shared_ptr<dist::Exponential>> _emiss) :
    Model(kVBHMM),
    num_states_{n_states},

    u_a_{std::move(_u_a)},
    u_pi_{std::move(_u_pi)},

    u_d_{std::move(_u_d)},
    w_a_(num_states_),
    emissions_{std::move(_emiss)}
{
  initializeDists();
  initialized = true;
}

VBHMM::VBHMM(McInt n_states,
             McInt n_syms,
             McFloat rate,
             McInt subchain_length,
             McInt buffer_length,
             McInt batch_size,
             bool full_step) :
    Model(kVBHMM),
    num_states_(n_states),
    learning_rate_(rate),
    subchain_length_(subchain_length),
    buffer_length_(buffer_length),
    batch_size_(batch_size),
    is_full_step_(full_step),
    u_d_(num_states_),
    w_a_(num_states_),
    emissions_(num_states_) {
  Eigen::VectorXf concentrations =
      ((Eigen::VectorXf::Random(n_states).array() + 1.001f
        ).array() * 0.28f).array() + 2.0f;
  u_a_.reset(new dist::Dirichlet(concentrations));
  u_pi_.reset(new dist::Dirichlet(concentrations));

  for (auto& p : u_d_) {
    auto concent =
        ((Eigen::VectorXf::Random(n_syms).array() + 1.001f
          ).array() * 0.2f).array() + 2.0f;
    p.reset(new dist::Dirichlet(concent));
  }

  for (auto& p : emissions_) {
    Eigen::VectorXf probs = Eigen::VectorXf::Random(n_syms).array() + 1.001f;
    probs.array() = probs.array() / probs.sum();
    p.reset(new dist::Multinoulli(probs));
  }

  initializeDists();
  initialized = true;
}

VBHMM::VBHMM(VBHMM const& other) :
    Model(kVBHMM),
    num_states_(other.num_states_),
    states_(other.states_),
    initialized(other.initialized){
  u_pi_.reset(new dist::Dirichlet(*(other.u_pi_)));
  u_pi_.reset(new dist::Dirichlet(*(other.u_pi_)));

  u_d_.resize(other.u_d_.size());
  for (auto ind : Range(0, u_d_.size())) {
    u_d_.at(ind).reset(new dist::Dirichlet(*(other.u_d_.at(ind))));
  }

  w_pi.reset(new dist::Dirichlet(*(other.w_pi)));

  w_a_.resize(other.w_a_.size());
  for (auto ind : Range(0, w_a_.size())) {
    w_a_.at(ind).reset(new dist::Dirichlet(*(other.w_a_.at(ind))));
  }
}

// Initialize w_a_, w_pi_, emissions.piror
void VBHMM::initializeDists() {
  for (auto& p : w_a_) {
    p.reset(new dist::Dirichlet(u_a_->getNatural().array() + 1.0f));
  }
  w_pi.reset(new dist::Dirichlet(u_pi_->getNatural().array() + 1.0f));

  for (auto k : Range(0, num_states_)) {
    (this->emissions_[k]->prior())->setNatural(u_d_[k]->getNatural());
  }
}

std::pair<McInt, McInt> VBHMM::genRange(
    McInt obs_size, McInt buf_size, McInt subchain_size) {
  std::mt19937 gen { RandomDevice::gen() };
  NIH_ASSERT_LT(buf_size, obs_size - subchain_size - buf_size)
      << '\n'
      << "Buffer size:" << buf_size << ", "
      << "Observation length: " << obs_size << ", "
      << "Subchain length: " << subchain_size;

  std::uniform_int_distribution<> dist(buf_size,
                                       obs_size - subchain_size - buf_size);
  McInt a = dist(gen);
  McInt b = a + subchain_size;
  return {a, b};
}

DistHiddenMarkovModel VBHMM::generateHMM(bool local, MatrixXfR const& obs) const {
  Timer t("generateHMM");
  MatrixXfR transit (num_states_, num_states_);

  for (auto ind : Range(0, num_states_)) {
    Eigen::VectorXf w = w_a_.at(ind)->getNatural();
    McFloat temp = boost::math::digamma(w.sum());
    for (auto k : Range(0, num_states_)) {
      // transit.row(ind)(k) = std::exp(boost::math::digamma(w[k]) - temp);
      transit.row(ind)(k) = boost::math::digamma(w[k]) - temp;
    }
  }

  Eigen::VectorXf pi (num_states_);
  if (local) {
    Eigen::EigenSolver<MatrixXfR> solver(transit);
    Eigen::VectorXf const& eigen_values = solver.eigenvalues().real();
    auto const& eigen_vectors = solver.eigenvectors().real();

    McInt max_ind =
        std::distance(eigen_values.data(),
                      std::max_element(eigen_values.data(),
                                       eigen_values.data() + eigen_values.size()));

    pi = eigen_vectors.col(max_ind);
    if (pi.minCoeff() < 0) {
      pi = - pi;
    }
    pi = (1 / pi.sum()) * pi;
  } else {
    Eigen::VectorXf const& w = w_pi->getNatural();
    McFloat temp = boost::math::digamma(w.sum());
    for (auto k : Range(0, num_states_)) {
      pi[k] = boost::math::digamma(w[k]) - temp;
    }
  }

  std::vector<std::shared_ptr<dist::Distribution>> dists;
  for (std::shared_ptr<dist::Exponential> const& d : emissions_) {
    std::shared_ptr<dist::Distribution> p_dist {d->genLogExpected()};
    dists.push_back(p_dist);
  }
  return DistHiddenMarkovModel(num_states_, transit, pi, dists, obs);
}

void VBHMM::fullVBStep(MatrixXfR const& obs) {
  NIH_ASSERT_EQ(obs.rows(), 1)
      << "Multiple sequences step is not supported by full VB.";
  states_ = generateHMM(false, obs);

  MatrixXfC const& transitions = states_.calcTransition().array().exp();
  for (auto k : Range(num_states_)) {
    Eigen::VectorXf const& new_row =
        u_a_->getNatural().array() + transitions.row(k).transpose().array();
    w_a_.at(k)->setNatural(new_row);
  }

  Eigen::array<McInt, 3> offsets = {0, 0, 0};
  Eigen::array<McInt, 3> extents = {1, obs.cols(), 1};
  Eigen::array<McInt, 1> const dims {{obs.cols()}};

// #pragma omp parallel for schedule(static)
//   for (McInt k = 0; k < num_states_; ++k) {
  for (auto k : Range(num_states_)) {
    auto dist = emissions_.at(k);
    Eigen::Tensor<McFloat, 3> const& gammas = states_.getGammas();
    offsets[2] = k;
    Eigen::Tensor<McFloat, 1> const& slice =
        gammas.slice(offsets, extents).reshape(dims);
    Eigen::VectorXf gammas_at_state_k(gammas.dimension(1));
    for (auto ind : Range(gammas.dimension(1))) {
      gammas_at_state_k(ind) = slice(ind);
    }
    auto suff = dist->calcExpectedSufficient(obs.row(0), gammas_at_state_k);
    MatrixXfC natural = suff.array() + u_d_.at(k)->getNatural().array();
    dist->prior()->setNatural(natural);
  }

  Eigen::VectorXf const& new_pi =
      u_pi_->getNatural().array() + states_.calcStart().array().exp();
  w_pi->setNatural(new_pi);

  states_ = generateHMM(false, obs);

  states_.eStep(obs);
}

void VBHMM::batchSVIStep(MatrixXfR const& obs) {
  McInt n_concentrations = emissions_.at(0)->prior()->getNatural().size();
  MatrixXfC emissions  = MatrixXfC::Zero(num_states_, n_concentrations);

  std::vector<std::pair<McInt, McInt>> subchains;
  subchains.reserve(batch_size_);

  for (McInt i = 0; i < batch_size_; ++i) {
    subchains.emplace_back(
        genRange(obs.cols(), buffer_length_, subchain_length_));
  }
  LOG(DEBUG) << "chain: [" << subchains[0].first << ", "
             << subchains[0].second << "]";

  states_ = generateHMM(true, obs);

  for (auto r_id : Range(0, obs.rows())) {
    for (auto m : Range(0, batch_size_)) {
      states_.eStepRowSubchain(r_id,
                               subchains.at(m).first, subchains.at(m).second,
                               buffer_length_, obs);
    }
  }

  // sufficient statistics
  MatrixXfC trans = MatrixXfR::Zero(num_states_, num_states_);
  Eigen::array<McInt, 3> offsets {0, 0, 0};
  Eigen::array<McInt, 3> extents {1, 0, 1};
  Eigen::array<McInt, 1> dims {{0}};
  for (auto ind : Range(0, batch_size_)) {
    auto const& subchain = subchains[ind];
    McInt const a = subchain.first;
    McInt const b = subchain.second;
    McInt const length = b - a + 1;
    Eigen::VectorXf gammas_at_state_k(length);
    Eigen::VectorXf x (length);

    MatrixXfC local_trans = states_.calcLocalTransition(a, b);
    trans.array() += Eigen::exp(local_trans.array());

    dims[0] = length;
    offsets[1] = a;
    extents[1] = length;
    for (auto j : Range(0, num_states_)) {
      offsets[2] = j;
      Eigen::Tensor<McFloat, 3> const& gammas = states_.getGammas();
      Eigen::Tensor<McFloat, 1> const& slice =
          gammas.slice(offsets, extents).reshape(dims);
      for (auto ind : Range(length)) {
        gammas_at_state_k(ind) = slice(ind);
      }
      for (auto ind : Range(length)) {
        x(ind) = obs.row(0)(a + ind);
      }

      Eigen::VectorXf suff =
          emissions_.at(j)->calcExpectedSufficient(x, gammas_at_state_k);
      emissions.row(j) += suff;
    }
  }

  // A
  McFloat c_a = (obs.cols() - subchain_length_ + 1);
  c_a /= subchain_length_ -1;
  for (auto ind : Range(0, num_states_)) {
    Eigen::VectorXf  const& new_row =
        this->u_a_->getNatural().array() +
        (c_a / batch_size_) * trans.row(ind).transpose().array();
    Eigen::VectorXf const& natural =
        (1.0f - learning_rate_) *
        this->w_a_[ind]->getNatural().array() + learning_rate_ * new_row.array();
    this->w_a_[ind]->setNatural(natural);
  }

  // prior of emissions
  McFloat c_phi = (obs.cols() - subchain_length_ + 1);
  c_phi /= subchain_length_;
  for (auto ind : Range(0, num_states_)) {
    std::shared_ptr<dist::Exponential>& dist = emissions_[ind];
    Eigen::VectorXf new_row
        = c_phi / static_cast<McFloat>(batch_size_) * emissions.row(ind).array();
    new_row.array() += u_d_[ind]->getNatural().array();
    Eigen::VectorXf temp = (1.0f - learning_rate_) * dist->prior()->getNatural();
    temp.array() += learning_rate_ * new_row.array();
    dist->prior()->setNatural(temp);
  }
}

McFloat VBHMM::elbo(MatrixXfR const& obs) const {
  McFloat res = - u_pi_->calcKLDivergence(w_pi.get());
  for (auto ind : Range(0, num_states_)) {
    auto emip = emissions_[ind]->prior();
    res -= u_a_->calcKLDivergence(w_a_[ind].get()) +
           u_d_[ind]->calcKLDivergence(emip.get());
  }
  res += states_.logLikelihood(obs);
  return res;
}

void VBHMM::step(MatrixXfR const& obs) {
  if (!initialized) {
    LOG(FATAL) << "Model VBHMM is not initialized.";
  }
  if (is_full_step_) {
    LOG(INFO) << "Running full step.";
    this->fullVBStep(obs);
  } else {
    LOG(INFO) << "Running SVI step.";
    this->batchSVIStep(obs);
  }
}

McFloat VBHMM::measure(MatrixXfR const& obs) const {
  return this->elbo(obs);
}

std::map<std::string,
         std::vector<std::shared_ptr<dist::Exponential const>>>
VBHMM::getAllDistributions() const {
  decltype(this->getAllDistributions()) res;
  res.insert({"u_a", {u_a_}});
  res.insert({"u_pi", {u_pi_}});

  std::vector<std::shared_ptr<dist::Exponential const>> u_d_casted (u_d_.size());
  for (auto i : Range(u_d_.size())) {
    std::shared_ptr<dist::Exponential> casted {
      cast<dist::Exponential>(dist::Distribution::create(u_d_[i]->getKindStr()))
    };
    casted->setNatural(u_d_[i]->getNatural());
    u_d_casted[i] = casted;
  }
  res.insert({"u_d", u_d_casted});
  res.insert({"w_pi", {w_pi}});

  std::vector<std::shared_ptr<dist::Exponential const>> w_a_casted (w_a_.size());
  for (auto i : Range(w_a_.size())) {
    std::shared_ptr<dist::Exponential> casted {
      cast<dist::Exponential>(dist::Distribution::create(w_a_[i]->getKindStr()))
    };
    casted->setNatural(w_a_[i]->getNatural());
    w_a_casted[i] = casted;
  }
  res.insert({"w_a", w_a_casted});

  std::vector<std::shared_ptr<dist::Exponential const>> emis_casted (emissions_.size());
  for (auto i : Range(emissions_.size())) {
    std::shared_ptr<dist::Exponential> casted {
      cast<dist::Exponential>(dist::Distribution::create(emissions_[i]->getKindStr()))
    };
    casted->setNatural(emissions_[i]->getNatural());
    emis_casted[i] = casted;
  }
  res.insert({"emissions", emis_casted});

  std::vector<std::shared_ptr<dist::Distribution>> state_emits_ =
      states_.getEmission();
  std::vector<std::shared_ptr<dist::Exponential const>>
      states_emis_casted (state_emits_.size());
  for (auto i : Range(state_emits_.size())) {
    std::shared_ptr<dist::Exponential> casted {
      cast<dist::Exponential>(dist::Distribution::create(state_emits_[i]->getKindStr()))};
    casted->setNatural(cast<dist::Exponential>(state_emits_[i].get())->getNatural());
    states_emis_casted[i] = casted;
  }
  res.insert({"state_emit", states_emis_casted});
  return res;
}

void VBHMM::save(nih::json::Json& handle) const {
  namespace js = nih::json;
  handle["num_states"] = js::Number(num_states_);
  handle["learning_rate"] = js::Number(learning_rate_);
  handle["subchain"] = js::Number(subchain_length_);
  handle["buffer"] = js::Number(buffer_length_);
  handle["batch"] = js::Number(batch_size_);
  handle["is_full_step"] = js::Boolean(is_full_step_);

  handle["u_a"] = js::Object();
  u_a_->save(handle["u_a"]);

  handle["u_pi"] = js::Object();
  u_pi_->save(handle["u_pi"]);

  // u_d
  std::vector<js::Json> u_d_json (u_d_.size());
  for (auto i : Range(u_d_json.size())) {
    u_d_json[i] = js::Object();
    u_d_[i]->save(u_d_json[i]);
  }
  handle["u_d"] = js::Array(std::move(u_d_json));

  // w_pi
  handle["w_pi"] = js::Object();
  w_pi->save(handle["w_pi"]);

  // w_a
  std::vector<js::Json> w_a_json (w_a_.size());
  for (auto i : Range(w_a_json.size())) {
    w_a_json[i] = js::Object();
    w_a_[i]->save(w_a_json[i]);
  }
  handle["w_a"] = js::Array(std::move(w_a_json));

  // emissions
  std::vector<js::Json> emissions_json (emissions_.size());
  for (auto i : Range(emissions_json.size())) {
    emissions_json[i] = js::Object();
    emissions_json[i]["name"] = emissions_[i]->getKindStr();
    emissions_json[i]["dist"] = js::Object();
    emissions_[i]->save(emissions_json[i]["dist"]);
  }
  handle["emissions"] = js::Array(std::move(emissions_json));

  // states
  handle["states"] = js::Object();
  states_.save(handle["states"]);
}

void VBHMM::load(nih::json::Json const& handle) {
  namespace js = nih::json;
  num_states_ = js::get<js::Number>(handle["num_states"]);
  learning_rate_ = js::get<js::Number>(handle["learning_rate"]);
  subchain_length_ = js::get<js::Number>(handle["subchain"]);
  buffer_length_ = js::get<js::Number>(handle["buffer"]);
  batch_size_ = js::get<js::Number>(handle["batch"]);
  is_full_step_ = js::get<js::Boolean>(handle["is_full_step"]);

  u_a_.reset(new dist::Dirichlet());
  u_a_->load(handle["u_a"]);

  u_pi_.reset(new dist::Dirichlet());
  u_pi_->load(handle["u_pi"]);

  // u_d
  std::vector<js::Json> u_d_json = js::get<js::Array>(handle["u_d"]);
  u_d_.resize(u_d_json.size());
  for (auto i : Range(u_d_json.size())) {
    u_d_[i].reset(new dist::Dirichlet());
    u_d_[i]->load(u_d_json[i]);
  }

  // w_pi
  w_pi.reset(new dist::Dirichlet());
  w_pi->load(handle["w_pi"]);

  // w_a
  std::vector<js::Json> w_a_json = js::get<js::Array>(handle["w_a"]);
  w_a_.resize(w_a_json.size());
  for (auto i : Range(w_a_json.size())) {
    w_a_[i].reset(new dist::Dirichlet());
    w_a_[i]->load(w_a_json[i]);
  }

  // emissions
  std::vector<js::Json> emissions_json = js::get<js::Array>(handle["emissions"]);
  emissions_.resize(emissions_json.size());
  for (auto i : Range(emissions_.size())) {
    std::string name = js::get<js::String>(emissions_json[i]["name"]);
    emissions_[i].reset(cast<dist::Exponential>(dist::Distribution::create(name)));
    emissions_[i]->load(emissions_json[i]["dist"]);
  }

  // states
  states_.load(handle["states"]);
}
}  // namespace model
}  // namespace mimic

KiwiAPI Kiwi_model Kiwi_VBHmmInit(
    int n_states, int n_symbols, float rate,
    int subchain_length, int buffer_length, int batch_size, int full_step) {
  mimic::model::Model* model = nullptr;
  model = new mimic::model::VBHMM(n_states, n_symbols, rate, subchain_length,
                                  buffer_length, batch_size, full_step);
  ASSERT(model) << "Failed to create VB Hidden Markov model.";
  return reinterpret_cast<Kiwi_model>(model);
}