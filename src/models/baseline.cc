#include <omp.h>

#include <vector>
#include <set>

#include <nih/logging.hh>

#include "mimic/data/Loader.hh"
#include "mimic/data/Matrix.hh"
#include "mimic/basic/strings.hh"
#include "mimic/models/baseline.hh"

namespace mimic {
namespace model {

McInt TracesTable::totalPossibleMismatches () const {
  return lookahead_len_ * (trace_len_ - (lookahead_len_ + 1) / 2);
}

void TracesTable::parse(Eigen::VectorXf const &sequence) {
  trace_len_ = sequence.size();
  this->total_possible_mismatches_ = this->totalPossibleMismatches();

  for (McInt window = 0; window < sequence.size(); window += 1) {
    Trace current_trace(lookahead_len_);
    size_t first_call = sequence[window];
    current_trace[0] = first_call;

    McInt i = 1;
    for (; i < lookahead_len_ &&
             window + i < static_cast<McInt>(sequence.size());
         ++i) {
      current_trace.at(i) = sequence[window+i];
    }

    if (i < lookahead_len_) {
      for (McInt j = i; j < lookahead_len_; ++j) {
        current_trace.at(j) = NAC;
      }
    }

    auto& call_set = this->traces_table_.at(first_call);
    call_set.insert(current_trace);
  }
}

void TracesTable::merge(TracesTable const& other) {
  for (size_t i = 0; i < kNCalls; ++i) {
    auto& traces_set_other = other.traces_table_.at(i);
    auto& traces_set_this = this->traces_table_.at(i);
    for (auto& trace : traces_set_other) {
      traces_set_this.insert(trace);
    }
  }
}

std::vector<TracesTable::TracesSet>& TracesTable::getTable() {
  return traces_table_;
}

size_t TracesTable::getTotalPossibleMismatches() const {
  if (total_possible_mismatches_ == 0) {
    LOG(FATAL) << "Table is empty.";
  }
  return total_possible_mismatches_;
}


/*
 *----------------------*
 *   BaseLineDetector   *
 *----------------------*
 */
BaseLineDetector::BaseLineDetector() : Model(kBaseline) {}

size_t BaseLineDetector::calcMismatch(TracesTable const& other) const {
  auto trace_mismatches =
      [this](TracesTable::TracesSet const& self,
             TracesTable::TracesSet const& other) {
        size_t n_mismatch = 0;
        size_t lookahead = this->table_.getLookAheadLength();

        for (TracesTable::Trace const& trace_other : other) {
          for (size_t c = 1; c < lookahead; ++c) {
            TracesTable::Call_t const call_other = trace_other.at(c);
            bool matched = false;
            for (TracesTable::Trace const& trace_self : self) {
              TracesTable::Call_t const call_self = trace_self.at(c);
              if (call_self == call_other) {
                matched = true;
                break;
              }
            }
            if (!matched) { n_mismatch++; }
          }
        }

        return n_mismatch;
      };

  int nthread = omp_get_max_threads();
  std::vector<size_t> mismatches (nthread, 0);

#pragma omp parallel num_threads(nthread)
  {
    size_t thread_mismatches = 0;
    int ithread = omp_get_thread_num();
#pragma omp for schedule(guided)
    for (TracesTable::Call_t call = 0; call < TracesTable::kNCalls; ++call) {
      TracesTable::TracesSet const& traces_set_self = table_.tracesSetAt(call);
      TracesTable::TracesSet const& traces_set_other = other.tracesSetAt(call);
      if (traces_set_self.size() == 0 && traces_set_other.size() == 0) {
        continue;
      }
      size_t n_mismatches = trace_mismatches(traces_set_self, traces_set_other);
      thread_mismatches += n_mismatches;
    }
    mismatches.at(ithread) = thread_mismatches;
  }

  size_t total_mismatches = 0;
  for (auto thread_mismatches : mismatches) {
    total_mismatches += thread_mismatches;
  }

  return total_mismatches;
};

void BaseLineDetector::step(MatrixXfR const& sequence) {
  NIH_ASSERT_EQ(sequence.rows(), 1)
      << "Multiple observation sequences is not supported.";
  table_.parse(sequence.row(0));
}

McFloat BaseLineDetector::measure(MatrixXfR const &sequence) const {
  NIH_ASSERT_EQ(sequence.rows(), 1)
      << "Multiple observation sequences is not supported.";
  TracesTable new_traces;
  new_traces.parse(sequence.row(0));
  if (table_.getLookAheadLength() != new_traces.getLookAheadLength()) {
    LOG(FATAL) << "Lookahead length mismatch.";
  }

  McFloat possible_mismatches = table_.getTotalPossibleMismatches();
  McFloat mismatches = calcMismatch(new_traces);
  McFloat miss_rate = mismatches / possible_mismatches;
  return miss_rate;
}

}  // namespace model
}  // namespace mimic