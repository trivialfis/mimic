#include <fstream>
#include <regex>
#include <string>

namespace mimic {
class Transformer {
 private:
  std::string const prefix;
  std::string const postfix;

 public:
  Transformer() : prefix{"opencl{"}, postfix{"}opencl"} {}
  ~Transformer() = default;

  void operator()(std::string src_file) {
    std::ifstream fin (src_file);
    if (!fin) {
      // LOG(FATAL) << "Source file: " << src_file << " not found";
    }

    std::string src_content;
    // fin.seekg(0, std::ios::end);
    // src_content.reserve(fin.tellg());
    // fin.seekg(0, std::ios::beg);

    std::string test_string = R"kernel_name(
#include "opencl{./src/kernel}opencl"
#include "opencl{./another}opencl"
)kernel_name";
    // src_content.assign((std::istreambuf_iterator<char>(fin)),
    //                    std::istreambuf_iterator<char>());
    src_content = test_string;

    std::string kernel_filename;

    std::regex e {"opencl\\{(.*)\\}opencl"};
    std::sregex_iterator next (src_content.cbegin(), src_content.cend(), e);
    std::sregex_iterator end;
    while (next != end) {
      std::smatch matches = *next;
      std::string kernel_path = matches[1].str();

      next++;
    }
  };
};
}  // namespace mimic

int main(int argc, char* const argv[]) {
  if (argc == 1) {
    // mimic::LOG(WARNING) << "Please input a filename";
    // return 1;
  }
  mimic::Transformer transformer;
  std::string ljkj = " ok ";
  transformer(ljkj);
  return 0;
}