#include <string>
#include <memory>

#include <nih/logging.hh>

#include "mimic/distributions/distribution.hh"
#include "mimic/distributions/dirichlet.hh"
#include "mimic/distributions/multinoulli.hh"

namespace mimic {
namespace dist {

std::string Distribution::getKindStr() const {
  switch (kind_) {
    case kDirichlet:
      return "dirichlet";
    case kMultinoulli:
      return "multinoulli";
    default:
      LOG(FATAL) << "Unknown distribution";
  }
  return "";
}

Distribution* Distribution::create(std::string name) {
  Distribution* ptr;
  if (name == "dirichlet") {
    ptr = new Dirichlet();
  } else if (name == "multinoulli") {
    ptr = new Multinoulli();
  } else {
    ptr = nullptr;
    LOG(FATAL) << "Unknown distribution.";
  }
  return ptr;
}

}  // namespace dist
}  // namespace mimic