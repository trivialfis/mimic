#include <algorithm>
#include <boost/math/special_functions/digamma.hpp>
#include <nih/logging.hh>

#include "mimic/distributions/multinoulli.hh"
#include "mimic/distributions/dirichlet.hh"
#include "mimic/basic/math.hh"
#include "mimic/basic/random.hh"
#include "mimic/basic/error.hh"

namespace mimic {
namespace dist {

Multinoulli::Multinoulli() : Exponential(kMultinoulli) {
  prior_.reset(new Dirichlet());
}

Multinoulli::Multinoulli(Eigen::VectorXf const& probs) :
    Exponential(kMultinoulli), probs_(probs),
    natural_cache_(new Cache<Eigen::VectorXf>) {
  McInt n_syms = probs_.size();
  natural_cache_->obsolate();
  // Dirichlet is the conjugate prior of category distribution.
  prior_.reset(new Dirichlet(Eigen::VectorXf::Ones(n_syms).array() / n_syms));
}

Eigen::VectorXf Multinoulli::getNatural() const {
  if (!natural_cache_->isnew()) {
    McInt n_syms = probs_.size();
    Eigen::VectorXf result(n_syms);
    for (McInt i : Range(n_syms)) {
      result[i] = probs_[i] / probs_[n_syms - 1];
    }
    result.array() = result.array().log();
    natural_cache_->update(std::move(result));
  }
  return natural_cache_->get();
}

void Multinoulli::setNatural(Eigen::VectorXf natural) {
  if (probs_.size() != 0) {
    NIH_ASSERT_EQ(natural.size(), probs_.size());
  }
  probs_ = Eigen::exp(natural.array()) / Eigen::exp(natural.array()).sum();
  natural_cache_->obsolate();
}

Eigen::VectorXf Multinoulli::getParameters() const {
  return probs_;
}

void Multinoulli::setParameters(Eigen::VectorXf const& probs) {
  natural_cache_->obsolate();
  probs_ = probs;
}

std::unique_ptr<Distribution> Multinoulli::genLogExpected() {
  Eigen::VectorXf w = this->prior()->getNatural();
  McFloat temp = boost::math::digamma(w.sum());

  Eigen::VectorXf probs (w.size());
  for (auto i : Range(w.size())) {
    probs[i] = boost::math::digamma(w[i]) - temp;
  }
  McFloat s = math::logSumExp(probs.data(), probs.data() + probs.size());
  probs = (probs.array() - s).exp();
  std::unique_ptr<Multinoulli> res {new Multinoulli(probs)};
  return res;
}

McFloat Multinoulli::logProbability(Eigen::VectorXf const& x) const {
  return std::log(this->probability(x));
}

McFloat Multinoulli::probability(Eigen::VectorXf const& x) const {
  NIH_ASSERT_EQ(x.size(), 1);
  NIH_ASSERT_LT(x[0], probs_.size());
  return this->probs_[x(0)];
}

MatrixXfC Multinoulli::calcExpectedSufficient(
    Eigen::VectorXf const& obs, Eigen::VectorXf const& states_probs) const {
  MatrixXfC res = MatrixXfC::Constant(probs_.size(), obs.size(),
                                      -std::numeric_limits<McFloat>::infinity());
  for (auto t : Range(obs.size())) {
    McInt out = static_cast<McInt>(obs(t));  // output category
    McFloat prob_emit = probs_(out);
    McFloat prob_at_current_state = states_probs(t);
    Eigen::VectorXf temp(2);
    temp[0] = prob_emit;
    temp[1] = prob_at_current_state;
    res(out, t) = math::logSumExp(temp.data(), temp.data() + temp.size());
  }
  return math::logSumExp(res, 1).array().exp();
}

Eigen::VectorXf Multinoulli::genSample() const {
  std::mt19937 gen { RandomDevice::gen() };
  std::vector<McInt> percentage(probs_.size());
  std::transform(probs_.data(), probs_.data() + probs_.size(),
                 percentage.begin(),
                 [](McFloat prob) { return prob * 100; });
  std::discrete_distribution<> dis(percentage.cbegin(), percentage.cend());
  Eigen::VectorXf res (1);
  res << dis(gen);
  return {res};
}

Eigen::VectorXf Multinoulli::mean(McInt n) const {
  return probs_.array() * n;
}

Eigen::VectorXf Multinoulli::mode() const {
  Eigen::VectorXf res (1);
  auto argmax_iter =
      std::max_element(probs_.data(), probs_.data() + probs_.size());
  res[0] = std::distance(probs_.data(), argmax_iter);;
  return res;
}

Eigen::VectorXf Multinoulli::variance() const {
  Eigen::VectorXf res(probs_.size());
  for (auto i : Range(res.size())) {
    res[i] = probs_[i] * (1 - probs_[i]);
  }
  return res;
}

MatrixXfC Multinoulli::covariance() const {
  McInt const size = probs_.size();
  MatrixXfC res(size, size);
  for (auto j : Range(size)) {
    for (auto i : Range(size)) {
      res(i, j) = - probs_[i] * probs_[j];
    }
  }
  return res;
}

void Multinoulli::save(nih::json::Json& handler) const {
  namespace js = nih::json;
  std::vector<js::Json> probabilities ( probs_.size() );
  for (auto i : Range(probs_.size())) {
    probabilities[i] = js::JsonNumber(probs_[i]);
  }
  handler["probabilities"] = js::JsonArray(probabilities);
  handler["prior"] = js::Object();
  prior_->save(handler["prior"]);
}

void Multinoulli::load(nih::json::Json const& handler) {
  std::vector<nih::json::Json> probabilities =
      nih::json::Cast<nih::json::JsonArray>(
          &handler["probabilities"].getValue())->getArray();
  probs_.resize(probabilities.size());
  for (auto i : Range(probabilities.size())) {
    probs_[i] =
        nih::json::Cast<nih::json::JsonNumber>(
            &probabilities[i].getValue())->getNumber();
  }
  prior_.reset(new Dirichlet());
  prior_->load(handler["prior"]);
}

ENSURE_METHODS_EXIST(Multinoulli)

}  // namespace dist
}  // namespace mimic