#include <mimic/basic.h>

#include <cmath>
#include <Eigen/Dense>
#include <boost/math/special_functions/digamma.hpp>
#include <nih/logging.hh>

#include "nih/json.hh"

#include "mimic/distributions/dirichlet.hh"
#include "mimic/basic/range.hh"
#include "mimic/basic/math.hh"
#include "mimic/basic/random.hh"

namespace mimic {
namespace dist {

Dirichlet::Dirichlet() : Exponential(kDirichlet) {}

Dirichlet::Dirichlet(Eigen::VectorXf const& alpha) :
    Exponential(kDirichlet), concentrations_(alpha.array() - 1.0f) {
  ASSERT(alpha.size() >= 2);
  Eigen::VectorXi w = (alpha.array() > 0).cast<int>();
  // FIXME: Enable only in debug mode.
  NIH_ASSERT_EQ(w.sum(), alpha.size()) << '\n' << w;
}

Eigen::VectorXf Dirichlet::getParameters() const {
  return concentrations_.array() + 1.0f;
}

void Dirichlet::setParameters(Eigen::VectorXf params) {
  concentrations_ = params.array() - 1.0f;
}

Eigen::VectorXf Dirichlet::getNatural() const {
  return concentrations_;
}

void Dirichlet::setNatural(Eigen::VectorXf natural) {
  if (concentrations_.size() != 0) {
    NIH_ASSERT_EQ(concentrations_.size(), natural.size());
  }
  concentrations_ = natural;
}

McFloat Dirichlet::probability(const Eigen::VectorXf &x) const {
  return std::exp(this->logProbability(x));
}

McFloat Dirichlet::logProbability(Eigen::VectorXf const& x) const {
  Eigen::VectorXf concentrations = concentrations_.array() + 1;
  NIH_ASSERT_EQ(x.size(), concentrations.size());
  NIH_ASSERT_LE(std::abs(x.sum() - 1.0f), kEps);
  McFloat sum {0};
  for (auto i : Range(concentrations.size())) {
    McFloat a = concentrations[i];
    McFloat t = x[i];
    sum += (a-1) * std::log(t);
  }
  McFloat result = sum - math::lbeta(concentrations);
  return result;
}

std::unique_ptr<Distribution> Dirichlet::genLogExpected() {
  LOG(FATAL) << "Not implemented";
  std::unique_ptr<Distribution> res { new Dirichlet() };
  return res;
}

MatrixXfC Dirichlet::calcExpectedSufficient(
    Eigen::VectorXf const& obs, Eigen::VectorXf const& states_probs) const {
  LOG(FATAL) << "Not implemented.";
  return MatrixXfC::Zero(0, 0);
}

McFloat Dirichlet::calcKLDivergence(Exponential const * const other) {
  Eigen::VectorXf const& other_params = other->getNatural().array() + 1;
  Eigen::VectorXf const& self_params = this->getNatural().array() + 1;

  McFloat const other_sum = other_params.sum();
  McFloat const self_sum = self_params.sum();

  McFloat result = std::lgamma(other_sum) - std::lgamma(self_sum);

  McFloat temp {0};
  for (auto i : Range(0, concentrations_.rows())) {
    temp += std::lgamma(other_params[i]) - std::lgamma(self_params[i]);
    temp -= (other_params[i] - self_params[i]) *
            (boost::math::digamma(other_params[i]) -
             boost::math::digamma(other_sum));
  }
  result -= temp;
  if (std::isnan(result) ||
      (result == std::numeric_limits<McFloat>::infinity())) {
    result = kEps;
  }
  return result;
}

Eigen::VectorXf Dirichlet::genSample() const {
  std::mt19937 gen { RandomDevice::gen() };
  Eigen::VectorXf sample (concentrations_.size());

  for (McInt i = 0; i < concentrations_.size(); ++i) {
    std::gamma_distribution<> dist {concentrations_[i] + 1, 1};
    McFloat s = dist(gen);
    sample[i] = s;
  }
  McFloat sum = sample.sum();
  sample.array() /= sum;
  return sample;
}

Eigen::VectorXf Dirichlet::mode() const {
  Eigen::VectorXf concentrations = concentrations_.array() + 1;
  auto numerator = concentrations.array() - 1;
  auto denominator = concentrations.sum() - concentrations.size();
  return numerator.array() / denominator;
}

Eigen::VectorXf Dirichlet::variance() const {
  Eigen::VectorXf concentrations = concentrations_.array() + 1;
  McFloat sum = concentrations.sum();
  auto numerator = concentrations .array() * (sum - concentrations.array());
  auto denominator = sum * sum * (sum + 1);
  return numerator.array() / denominator;
}

MatrixXfC Dirichlet::covariance() const {
  Eigen::VectorXf const& concent = getParameters();
  McFloat const sum = concent.sum();
  McFloat const denominator = sum * sum * (sum + 1);
  McInt const size = concent.size();
  MatrixXfC res (size, size);
  for (auto j : Range(size)) {
    for (auto i : Range(size)) {
      McFloat numerator = - concent[i] * concent[j];
      McFloat value = numerator / denominator;
      res(i, j) = value;
    }
  }
  return res;
}

Eigen::VectorXf Dirichlet::mean() const {
  // no need to +1
  Eigen::VectorXf res(getNatural());
  McFloat c_sum = getNatural().sum();
  res.array() /= c_sum;
  return res;
}

void Dirichlet::save(nih::json::Json& handler) const {
  std::vector<nih::json::Json> concentrats ( concentrations_.size() );
  for (auto i : Range(concentrations_.size())) {
    concentrats[i] = nih::json::Number(concentrations_[i]);
  }
  handler["concentrations"] = nih::json::JsonArray(concentrats);
}

void Dirichlet::load(nih::json::Json const& handler) {
  namespace js = nih::json;
  std::vector<js::Json> const& concentrats =
      js::get<js::Array const>(handler["concentrations"]);
  concentrations_.resize(concentrats.size());
  for (auto i : Range(concentrations_.size())) {
    concentrations_[i] = js::get<js::Number const>(concentrats[i]);
  }
}

ENSURE_METHODS_EXIST(Dirichlet)
}  // namespace dist
}  // namespace mimic