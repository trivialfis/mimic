``` bibtex
@INPROCEEDINGS{502675,
author={S. Forrest and S. A. Hofmeyr and A. Somayaji and T. A. Longstaff},
booktitle={Proceedings 1996 IEEE Symposium on Security and Privacy},
title={A sense of self for Unix processes},
year={1996},
volume={},
number={},
pages={120-128},
keywords={Unix;security of data;living systems;Unix processes;anomaly detection;computer security systems;immune systems;UNIX programs;Immune system;Computer security;Protection;Computer science;Operating systems;Cryptography;Skin;Biomembranes;Robustness;Software engineering},
doi={10.1109/SECPRI.1996.502675},
ISSN={1081-6011},
month={May},}
```

``` bibtex
@article{article,
author = {Rabiner, Lawrence},
year = {1989},
month = {03},
pages = {257 - 286},
title = {A Tutorial on Hidden Markov Models and Selected Applications on Speech Recognition},
volume = {77},
journal = {Proceedings of the IEEE},
doi = {10.1109/5.18626}
}
```

``` bibtex
@dataset{dataset,
author = {Stamp, Mark and Professor, Associate},
year = {2013},
month = {10},
pages = {},
title = {A Revealing Introduction to Hidden Markov Models}
}
```

``` bibtex
@article{article,
author = {Ramage, Daniel},
year = {2019},
month = {01},
pages = {},
title = {Hidden Markov Models Fundamentals}
}
```

``` bibtex
@INPROCEEDINGS{1048264,
author={R. I. A. Davis and B. C. Lovell and T. Caelli},
booktitle={Object recognition supported by user interaction for service robots},
title={Improved estimation of hidden Markov model parameters from multiple observation sequences},
year={2002},
volume={2},
number={},
pages={168-171 vol.2},
keywords={pattern recognition;hidden Markov models;parameter estimation;learning (artificial intelligence);probability;estimation theory;hidden Markov models;parameter estimation;multiple observation sequences;pattern recognition;observation sequences;Baum-Welch reestimation procedure;probability;Hidden Markov models;Face recognition;Handwriting recognition;Pattern recognition;Speech recognition;Artificial intelligence;Probability},
doi={10.1109/ICPR.2002.1048264},
ISSN={1051-4651},
month={Aug},}
```

``` bibtex
@ARTICLE{2017arXiv170501064L,
       author = {{Ly}, Alexander and {Marsman}, Maarten and {Verhagen}, Josine and
        {Grasman}, Raoul and {Wagenmakers}, Eric-Jan},
        title = "{A Tutorial on Fisher Information}",
      journal = {arXiv e-prints},
     keywords = {Mathematics - Statistics Theory, 62-01, 62B10 (Primary), 62F03, 62F12, 62F15, 62B10 (Secondary)},
         year = 2017,
        month = May,
          eid = {arXiv:1705.01064},
        pages = {arXiv:1705.01064},
archivePrefix = {arXiv},
       eprint = {1705.01064},
 primaryClass = {math.ST},
       adsurl = {https://ui.adsabs.harvard.edu/\#abs/2017arXiv170501064L},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

```bibtex
@article{article,
author = {James. Beal, Matthew},
year = {2003},
month = {01},
pages = {},
title = {Variational algorithms for approximate Bayesian inference /},
journal = {PhD thesis}
}
```

``` bibtex
@article{DBLP:journals/corr/Martens14,
  author    = {James Martens},
  title     = {New perspectives on the natural gradient method},
  journal   = {CoRR},
  volume    = {abs/1412.1193},
  year      = {2014},
  url       = {http://arxiv.org/abs/1412.1193},
  archivePrefix = {arXiv},
  eprint    = {1412.1193},
  timestamp = {Mon, 13 Aug 2018 16:46:55 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/Martens14},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```

```
@article{article,
author = {Johnson, M.J. and Willsky, A.S.},
year = {2014},
month = {01},
pages = {3872-3880},
title = {Stochastic variational inference for Bayesian time series models},
volume = {5},
journal = {31st International Conference on Machine Learning, ICML 2014}
}
```