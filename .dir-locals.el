((c++-mode
  (require-final-newline . nil))
 (cmake-mode
  (require-final-newline . nil))
 (emacs-lisp-mode
  (require-final-newline . nil))
 (python-mode
  (require-final-newline . nil))
 (scheme-mode
  (require-final-newline . nil)))

;; Local Variables:
;; flycheck-disabled-checkers: (emacs-lisp-checkdoc emacs-lisp)
;; End: