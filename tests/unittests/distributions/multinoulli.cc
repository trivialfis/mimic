#include <gtest/gtest.h>
#include <vector>
#include <Eigen/Dense>
#include <memory>

#include "mimic/basic.h"
#include "mimic/basic/range.hh"
#include "mimic/distributions/multinoulli.hh"

namespace mimic {
namespace dist {

TEST(Multinoulli, Natural) {
  Eigen::VectorXf probs (4);
  probs << 0.25, 0.25, 0.25, 0.25;
  Multinoulli dist(probs);

  std::vector<McFloat> natural_sol {0.f, 0.f, 0.f, 0.f};
  auto natural = dist.getNatural();
  for (auto i : Range(4)) {
    ASSERT_NEAR(natural_sol[i], natural[i], kEps);
  }
}

TEST(Multinoulli, Sufficient) {
  Eigen::VectorXf probs (4);
  probs << 0.20, 0.25, 0.25, 0.30;
  Multinoulli dist(probs);

  Eigen::VectorXf obs (4);
  obs << 0, 1, 2, 3;
  Eigen::VectorXf states_probs(4);
  states_probs << 0.20, 0.25, 0.25, 0.30;

  Eigen::VectorXf res =
      dist.calcExpectedSufficient(obs, states_probs).col(0);

  std::vector<McFloat> sol {2.44280552,
                            2.56805,
                            2.56805,
                            2.69971776};
  for (auto i : Range(res.size())) {
    ASSERT_NEAR(sol[i], res[i], kEps);
  }
}

TEST(Multinoulli, Mean) {
  Eigen::VectorXf probs (4);
  probs << 0.20, 0.25, 0.25, 0.30;
  Multinoulli dist(probs);
  auto value = dist.mean(3);
  probs.array() *= 3;
  for (auto i : Range(probs.size())) {
    ASSERT_NEAR(probs[i], value[i], kEps);
  }
}

TEST(Multinoulli, Mode) {
  Eigen::VectorXf probs (4);
  probs << 0.20, 0.25, 0.25, 0.30;
  Multinoulli dist(probs);
  McInt mode = dist.mode()[0];
  ASSERT_EQ(mode, 3);
}

TEST(Multinoulli, Variance) {
  Eigen::VectorXf probs (4);
  probs << 0.20, 0.25, 0.25, 0.30;
  Multinoulli dist(probs);
  auto variance = dist.variance();
  std::vector<McFloat> variance_sol {
    0.16,
    0.1875,
    0.1875,
    0.21
  };
  for (auto i : Range(variance_sol.size())) {
    ASSERT_NEAR(variance[i], variance_sol[i], kEps);
  }
}

TEST(Multinoulli, Covariance) {
  Eigen::VectorXf probs (4);
  probs << 0.20, 0.25, 0.25, 0.30;
  Multinoulli dist(probs);
  auto covariance = dist.covariance();

  MatrixXfC covariance_sol (probs.size(), probs.size());
  covariance_sol <<
      -0.04,   -0.05,   -0.05,   -0.06,
      -0.05, -0.0625, -0.0625,  -0.075,
      -0.05, -0.0625, -0.0625,  -0.075,
      -0.06,  -0.075,  -0.075,   -0.09;
  for (auto i : Range(covariance.size())) {
    ASSERT_NEAR(covariance_sol.data()[i], covariance.data()[i], kEps);
  }
}

TEST(Multinoulli, IO) {
  nih::json::Json handler { nih::json::JsonObject() };
  Eigen::VectorXf natural_saved;
  {
    Eigen::VectorXf probabilities (4);
    probabilities << 0.2, 0.3, 0.2, 0.3;
    Multinoulli dist(probabilities);

    natural_saved = dist.getNatural();
    dist.save(handler);
  }

  Eigen::VectorXf natural_loaded;
  {
    Eigen::VectorXf probabilities (3);
    probabilities << 0.2, 0.2, 0.6;
    Multinoulli dist(probabilities);
    dist.load(handler);
    natural_loaded = dist.getNatural();
  }

  for (auto i : Range(natural_saved.size())) {
    ASSERT_NEAR(natural_saved[i], natural_loaded[i], kEps);
  }
}

}  // namespace dist
}  // namespace mimic