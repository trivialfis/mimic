#include <gtest/gtest.h>

#include "nih/json.hh"

#include "mimic/basic.h"
#include "mimic/basic/range.hh"
#include "mimic/basic/random.hh"
#include "mimic/distributions/dirichlet.hh"

namespace mimic {
namespace dist {

TEST(Dirichlet, Basic) {
  Eigen::VectorXf concentrations (4);
  concentrations << 0.2, 0.3, 0.2, 0.3;
  EXPECT_NO_THROW(Dirichlet{concentrations});

  concentrations << 0.1, 0.1, -0.1, 0.1;
  EXPECT_ANY_THROW(Dirichlet{concentrations});
}

TEST(Dirichlet, Mean) {
  Eigen::VectorXf concentrations (2);
  concentrations << 2.0, 2.0;
  auto dist = Dirichlet(concentrations);
  auto d_mean = dist.mean();
  for (auto i : Range(2)) {
    ASSERT_NEAR(d_mean(i), 0.5, kEps);
  }
}

TEST(Dirichlet, Mode) {
  Eigen::VectorXf concentrations (2);
  concentrations << 2.0, 2.0;
  auto dist = Dirichlet(concentrations);
  auto mode = dist.mode();
  for (auto i : Range(mode.size())) {
    ASSERT_NEAR(mode[i], 0.5, kEps);
  }
}

TEST(Dirichlet, Variance) {
  Eigen::VectorXf concentrations (2);
  concentrations << 2.0, 2.0;
  auto dist = Dirichlet(concentrations);
  auto var = dist.variance();
  for (auto i : Range(2)) {
    ASSERT_NEAR(var(i), 0.05f, kEps);
  }
}

TEST(Dirichlet, Covariance) {
  Eigen::VectorXf concentrations (2);
  concentrations << 2.0, 2.0;
  auto dist = Dirichlet(concentrations);
  auto covar = dist.covariance();
  for (auto i : Range(covar.size())) {
    ASSERT_NEAR(covar.data()[i], -0.05, kEps);
  }
}

TEST(Dirichlet, Probability) {
  Eigen::VectorXf concentrations (2);
  concentrations << 2.0, 2.0;
  auto dist = Dirichlet(concentrations);

  Eigen::VectorXf obs(2);
  obs << 0.3, 0.7;
  McFloat log_res = dist.logProbability(obs);
  ASSERT_NEAR(log_res, 0.231111720963386, kEps);

  McFloat res = dist.probability(obs);
  ASSERT_NEAR(std::exp(log_res), res, kEps);
}

TEST(Dirichlet, GenSample) {
  RandomContext context(1);

  Eigen::VectorXf concentrations (4);
  concentrations << 0.2, 0.3, 0.2, 0.3;
  Dirichlet dist(concentrations);
  Eigen::VectorXf sample = dist.genSample();

  ASSERT_NEAR(sample[0], 0.180141, kEps);
  ASSERT_NEAR(sample[1], 0.027411, kEps);
  ASSERT_NEAR(sample[2], 0.085433, kEps);
  ASSERT_NEAR(sample[3], 0.707015, kEps);
}

TEST(Dirichlet, KL) {
  {
    Eigen::VectorXf cons[2] = {
      Eigen::VectorXf(4),
      Eigen::VectorXf(4)
    };
    cons[0] << 0.2, 0.3, 0.2, 0.3;
    cons[1] << 0.2, 0.3, 0.2, 0.3;

    Dirichlet a = Dirichlet(cons[0]);
    Dirichlet b = Dirichlet(cons[1]);
    ASSERT_EQ(a.calcKLDivergence(&b), 0);
  }

  {
    Eigen::VectorXf cons[2] = {
      Eigen::VectorXf(2),
      Eigen::VectorXf(2)
    };

    cons[0] << 0.4, 0.6;
    cons[1] << 0.5, 0.5;
    Dirichlet a = Dirichlet(cons[0]);
    Dirichlet b = Dirichlet(cons[1]);
    ASSERT_NEAR(a.calcKLDivergence(&b),  0.05018178992f, kEps);
  }
}

TEST(Dirichlet, IO) {
  nih::json::Json handler { nih::json::Object() };
  Eigen::VectorXf natural_saved;
  {
    Eigen::VectorXf concentrations (4);
    concentrations << 0.2, 0.3, 0.2, 0.3;
    Dirichlet dist(concentrations);

    natural_saved = dist.getNatural();
    dist.save(handler);
  }

  Eigen::VectorXf natural_loaded;
  {
    Eigen::VectorXf concentrations (3);
    concentrations << 0.2, 0.2, 0.6;
    Dirichlet dist(concentrations);
    dist.load(handler);
    natural_loaded = dist.getNatural();
  }

  for (auto i : Range(natural_saved.size())) {
    ASSERT_NEAR(natural_saved[i], natural_loaded[i], kEps);
  }
}

}  // namespace dist
}  // namespace mimic