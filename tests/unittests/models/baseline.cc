#include <gtest/gtest.h>

#include <fstream>
#include <memory>

#include "mimic/models/baseline.hh"
#include "mimic/basic/FileSystem.hh"

namespace mimic {
namespace model {

template<typename Derived, typename Base>
std::unique_ptr<Derived>
dynamic_unique_ptr_cast( std::unique_ptr<Base>&& p ) {
  if(Derived *result = dynamic_cast<Derived *>(p.get())) {
    p.release();
    return std::unique_ptr<Derived>(result);
  }
  return std::unique_ptr<Derived>(nullptr);
}

TEST(TracesTable, Load) {
  TracesTable table;

  TemporaryDirectory tmp_dir;
  auto dir_path = tmp_dir.GetPath();

  std::string filename = "/TracesTable-Load.txt";
  filename = dir_path + filename;

  std::ofstream fout(filename);
  fout << "0 1 2 3 4 5 2 1 2 1";
  fout << std::endl;
  fout.close();

  auto vec = LoadToEigenVector(Path{filename});

  table.parse(vec);

  auto& table_raw = table.getTable();

  std::vector<TracesTable::TracesSet> solution;
  solution.push_back({{0, 1, 2}});
  solution.push_back({{1, 2, 3}, {1, 2, 1},
                      {1, TracesTable::NAC, TracesTable::NAC}});
  solution.push_back({{2, 3, 4},
                      {2, 1, 2},
                      {2, 1, TracesTable::NAC}});
  solution.push_back({{3, 4, 5}});
  solution.push_back({{4, 5, 2}});
  solution.push_back({{5, 2, 1}});

  for (size_t i = 0; i < 5; ++i) {
    ASSERT_EQ(solution[i], table_raw.at(i));
  }
}

TEST(TracesTable, Merge) {
  TracesTable table[2];
  TemporaryDirectory tmp_dir[2];

  std::string samples[2] = {"0 1 2 3 4 5 2 1 2 1",
                            "0 1 3"};

  for (size_t i = 0; i < 2; ++i) {
    auto dir_path = tmp_dir[i].GetPath();
    std::string filename = "/TracesTable-Load_" + std::to_string(i) + ".txt";
    filename = dir_path + filename;
    std::ofstream fout(filename);
    fout << samples[i];
    fout << std::endl;
    fout.close();

    auto vec = LoadToEigenVector(Path{filename});

    table[i].parse(vec);
  }

  table[0].merge(table[1]);

  auto table_raw = table[0].getTable();

  TracesTable::Table solution = {
    {{0, 1, 2}, {0, 1, 3}},
    {{1, 2, 3}, {1, 2, 1}, {1, 3, TracesTable::NAC},
     {1, TracesTable::NAC, TracesTable::NAC}},
    {{2, 3, 4},
     {2, 1, 2},
     {2, 1, TracesTable::NAC}},
    {{3, 4, 5}, {3, TracesTable::NAC, TracesTable::NAC}},
    {{4, 5, 2}},
    {{5, 2, 1}}
  };

  for (size_t i = 0; i < 5; ++i) {
    ASSERT_EQ(solution.at(i), table_raw.at(i));
  }
}

TEST(BaseLineDetector, Score) {
  std::unique_ptr<Model> bl_detector { new BaseLineDetector() };
  TemporaryDirectory tmp_dir;

  auto dir_path = tmp_dir.GetPath();
  std::string filename[2] = {"/TracesTable-Load_0.txt", "/TracesTable-Load_1.txt"};
  for (size_t i = 0; i < 2; ++i) {
    filename[i] = dir_path + filename[i];
  }

  std::ofstream fout(filename[0]);
  fout << "0 1 2 3 4 5 2 1 2 1";
  fout << std::endl;
  fout.close();

  fout.open(filename[1]);
  fout << "0 3 2 3 4 5 2 1 2 1";
  fout.close();

  testing::internal::CaptureStdout();
  auto train_vec = LoadToEigenVector(Path{filename[0]});
  std::string captuered_stdout = testing::internal::GetCapturedStdout();
  ASSERT_NE(captuered_stdout.find("Loading from file"), std::string::npos);


  MatrixXfR train_obs(1, train_vec.size());
  for (auto i : Range(train_vec.size())) {
    train_obs(0, i) = train_vec[i];
  }
  bl_detector->step(train_obs);

  testing::internal::CaptureStdout();
  auto vec = LoadToEigenVector(Path{filename[1]});
  captuered_stdout = testing::internal::GetCapturedStdout();
  ASSERT_NE(captuered_stdout.find("Loading from file"), std::string::npos);

  TracesTable table;

  auto nac = TracesTable::NAC;
  TracesTable::Table raw_table_sol = {
    {{0, 3, 2}},
    {{1, 2, 1}, {1, nac, nac}},
    {{2, 3, 4}, {2, 1, 2}, {2, 1, nac}},
    {{3, 2, 3}, {3, 4, 5}},
    {{4, 5, 2}},
    {{5, 2, 1}}
  };

  table.parse(vec);
  auto raw_table = table.getTable();
  for (size_t i = 0; i < 5; ++i) {
    ASSERT_EQ(raw_table[i], raw_table_sol[i]);
  }

  MatrixXfR measure_obs (1, vec.size());
  for (auto i : Range(vec.size())) {
    measure_obs(0, i) = vec[i];
  }
  McFloat score {bl_detector->measure(measure_obs)};
  ASSERT_EQ(0.125f, score);
}

}  // namespace model
}  // namespace mimic