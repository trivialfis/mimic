#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <numeric>
#include <random>

#include "mimic/basic.h"
#include "mimic/basic/FileSystem.hh"
#include "mimic/models/hmm.hh"

namespace mimic {
namespace model {

class HiddenMarkovModelMock : public HiddenMarkovModel {
 public:
  HiddenMarkovModelMock() : HiddenMarkovModel(2, 8) {
    HiddenMarkovModel::transition_ = Eigen::MatrixXf::Constant(2, 2, 0.5f);
    HiddenMarkovModel::emission_ = Eigen::MatrixXf::Constant(2, 8, 1/8.0f);
  }
  HiddenMarkovModelMock(McInt n_states, McInt n_symbols) :
      HiddenMarkovModel(n_states, n_symbols) {}

  HiddenMarkovModelMock(HiddenMarkovModel::Parameter _param):
      HiddenMarkovModel::HiddenMarkovModel(_param) {}

  auto estimateStates(Eigen::VectorXf const& sequence) ->
      decltype(HiddenMarkovModel::estimateStates(sequence)){
    auto gammas_pair =
        HiddenMarkovModel::estimateStates(sequence);
    auto& gammas = gammas_pair.first;
    auto& digammas = gammas_pair.second;
    return std::make_pair(gammas, digammas);
  }
  auto forward(Eigen::VectorXf const &sequence) ->
      decltype(HiddenMarkovModel::forward(sequence)){
    MatrixXfC result =
        HiddenMarkovModel::forward(sequence);
    return result;
  }
  auto backward(Eigen::VectorXf const &sequence) ->
      decltype(HiddenMarkovModel::backward(sequence)){
    MatrixXfC result =
        HiddenMarkovModel::backward(sequence);
    return result;
  }
};

TEST(HMM, Forward) {
  HiddenMarkovModelMock mock(2, 16);

  Eigen::VectorXf vec(16);
  vec << 13, 6, 1, 4, 12, 4, 8, 10, 4, 6, 3, 5, 8, 7, 9, 9;
  auto forward_matrix = mock.forward(vec);
  MatrixXfC solution(2, 16);
  for (McInt c = 0; c < 16; ++c) {
    for (McInt r = 0; r < 2; ++r) {
      solution(r, c) = 0.5f;
    }
  }
  ASSERT_EQ(forward_matrix, solution);
}

TEST(HMM, Backward) {
  HiddenMarkovModelMock mock(2, 16);

  Eigen::VectorXf vec(16);
  vec << 13, 6, 1, 4, 12, 4, 8, 10, 4, 6, 3, 5, 8, 7, 9, 9;
  mock.forward(vec);
  MatrixXfC result = mock.backward(vec);
  for (size_t i = 0; i < result.rows(); ++i) {
    for (size_t j = 0; j < result.cols(); ++j) {
      ASSERT_EQ(result(i, j), 16);
    }
  }
}

TEST(HMM, Estimate) {
  HiddenMarkovModelMock mock;

  Eigen::VectorXf vec(8);
  vec << 0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f;
  auto result = mock.estimateStates(vec);
  for (size_t r = 0; r < result.first.rows(); ++r) {
    for (size_t c = 0; c < result.first.cols(); ++c) {
      ASSERT_EQ(result.first(r, c), 0.5);
    }
  }
}

TEST(HMM, Train) {
  size_t constexpr size = 16;
  HiddenMarkovModelMock mock(4, 16);

  MatrixXfR vec(1, size);
  vec << 13, 6, 1, 4, 12, 4, 8, 10, 4, 6, 3, 5, 8, 7, 9, 9;
  {
    mock.step(vec);
    McFloat score = mock.measure(vec);

    ASSERT_GT(score, -45.0f);
  }

  {
    HiddenMarkovModel::Parameter param;
    param.SetNumStates(2);
    HiddenMarkovModelMock mock(param);

    mock.step(vec);
    McFloat score = mock.measure(vec);

    ASSERT_GT(score, -37.0f);
  }
}

#if defined(ENABLE_PROFILING) && ENABLE_PROFILING == 1
TEST(HMM, Benchmark) {
  size_t constexpr size = 1600000;
  size_t constexpr n_states = 32;
  size_t constexpr n_symbols = 340;
  // size_t constexpr size = 16;
  std::vector<McInt> data (size);
  HiddenMarkovModelMock mock(n_states, n_symbols);

  std:iota(data.begin(), data.end(), 0);
  data[0] = 12;
  data[13] = 3;

  Eigen::VectorXf vec(size);
  for (size_t i = 0; i < data.size(); ++i) {
    vec[i] = data[i] % n_symbols;
  }
  {
    mock.Train(vec);
    McFloat score = mock.Score(vec);
  }
}
#endif  // defined(ENABLE_PROFILING) && ENABLE_PROFILING == 1

}  // namespace model
}  // namespace mimic