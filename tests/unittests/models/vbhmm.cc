#include <gtest/gtest.h>

#include <tuple>
#include <memory>
#include <fstream>
#include <sstream>

#include "nih/json.hh"

#include "mimic/basic/random.hh"
#include "mimic/distributions/dirichlet.hh"
#include "mimic/distributions/multinoulli.hh"
#include "mimic/models/vbhmm.hh"

namespace mimic {
namespace model {

/*! \brief Mock class used to access internal of VBHMM. */
class VBHMMMock : protected VBHMM {
 public:
  static std::tuple<McInt,
             std::shared_ptr<dist::Dirichlet>,
             std::shared_ptr<dist::Dirichlet>,
             std::vector<std::shared_ptr<dist::Dirichlet>>,
             std::vector<std::shared_ptr<dist::Exponential>>> constructDists() {
    McInt constexpr num_states = 2;
    Eigen::VectorXf concentrations(num_states);
    concentrations << 3.0, 3.0;

    std::shared_ptr<dist::Dirichlet> u_a {
      new dist::Dirichlet(concentrations)
    };
    std::shared_ptr<dist::Dirichlet> u_pi {
      new dist::Dirichlet(concentrations)
    };

    std::vector<std::shared_ptr<dist::Dirichlet>> u_d (num_states);
    for (auto& p : u_d) {
      p.reset(new dist::Dirichlet(concentrations));
    }

    Eigen::VectorXf probs(2);
    probs << 0.5, 0.5;
    std::vector<std::shared_ptr<dist::Exponential>> emits (num_states);
    for (auto& p : emits) {
      p.reset(new dist::Multinoulli(probs));
    }
    return std::make_tuple(num_states, u_a, u_pi, u_d, emits);
  }

 private:
  RandomContext context;
  FRIEND_TEST(VBHMM, BatchSVIStepDefault);
  FRIEND_TEST(VBHMM, GenRange);
  FRIEND_TEST(VBHMM, GenHMM);
  FRIEND_TEST(VBHMM, BatchSVIStep);
  FRIEND_TEST(VBHMM, SVIStepReproduce);

  FRIEND_TEST(VBHMM, FullVBStep);
  FRIEND_TEST(VBHMM, FullVBStepReproduce);
  FRIEND_TEST(VBHMM, IO);

 public:
  VBHMMMock() : context(11) {}
  VBHMMMock(McInt n_states, McInt n_symbols,
            McFloat rate,
            McInt subchain_length=0, McInt buffer_length=0,
            McInt batch_size=1,
            bool full_step=false) :
      context(11),
      VBHMM(n_states, n_symbols, rate,
            subchain_length, buffer_length,
            batch_size, full_step) {}

  void construct() {
    auto params = constructDists();

    num_states_ = std::get<0>(params);
    u_a_ = std::get<1>(params);
    u_pi_ = std::get<2>(params);
    u_d_ = std::get<3>(params);
    emissions_ = std::get<4>(params);
    w_a_.resize(num_states_);

    initializeDists();
  }

  MatrixXfR constructWithHMM() {
    MatrixXfR obs (1, 10);
    obs << 0., 0., 1., 0., 0., 0., 0., 1., 0., 0.;

    construct();
    DistHiddenMarkovModel const& hmm = generateHMM(false, obs);

    VBHMM::states_ = hmm;
    VBHMM::states_.eStep(obs);
    return obs;
  }
};

TEST(VBHMM, GenRange) {
  VBHMMMock vbhmm;
  auto range = vbhmm.genRange(100, 10, 10);
  ASSERT_EQ(range.second - range.first, 10);
}

// FIXME(trivialfis): Test this.
TEST(VBHMM, Construct) {
  MatrixXfR obs = MatrixXfR::Random(2, 100);
  auto params = VBHMMMock::constructDists();

  VBHMM learner (std::get<0>(params),
                 std::get<1>(params),
                 std::get<2>(params),
                 std::get<3>(params),
                 std::get<4>(params));
}

// FIXME(trivialfis): Test this
TEST(VBHMM, InitializeDists) {
  VBHMMMock mock;
  // FIXME(trivialfis); Test this.
}

TEST(VBHMM, GenHMM) {
  MatrixXfR obs(1, 5);
  obs << 0, 0, 1, 0, 0;

  auto constructHMM =
      [&](bool local) {
        VBHMMMock learner;
        learner.construct();
        DistHiddenMarkovModel hmm = learner.generateHMM(local, obs);
        return hmm;
      };

  {
    DistHiddenMarkovModel hmm = constructHMM(false);
    ASSERT_EQ(hmm.getNumHidden(), 2);
    ASSERT_LE((hmm.getStart().array() + 0.83333333).array().abs().sum(), kEps);
    ASSERT_LE((hmm.getTransition().array() + 0.83333333).array().abs().sum(),
              kEps);
    // FIXME(trivialfis): Test emissions.
  }

  {
    DistHiddenMarkovModel hmm = constructHMM(true);
    ASSERT_EQ(hmm.getNumHidden(), 2);
    ASSERT_LE((hmm.getStart().array() + 0.693147).array().abs().sum(), kEps);
    ASSERT_LE((hmm.getTransition().array() + 0.83333333).array().abs().sum(),
              kEps);
    // FIXME(trivialfis): Test emissions.
  }
}

TEST(VBHMM, BatchSVIStep) {
  VBHMMMock learner;
  auto obs = learner.constructWithHMM();
  learner.learning_rate_ = 0.5;
  learner.subchain_length_ = 4;
  learner.buffer_length_ = 2;
  learner.batch_size_ = 1;
  learner.batchSVIStep(obs);

  McInt k_states = learner.num_states_;
  std::vector<std::shared_ptr<dist::Exponential>> emissions =
      learner.emissions_;
  Eigen::VectorXf emis_natural(2);
  emis_natural << 0.0f, 0.0f;
  for (auto& p : emissions) {
    ASSERT_LE((p->getNatural() - emis_natural).array().abs().sum(), kEps);
  }
  std::vector<std::shared_ptr<dist::Dirichlet>> w_a =
      learner.w_a_;
  Eigen::VectorXf w_a_natural (2);
  w_a_natural << 3.16666666f, 3.16666666;
  for (auto& p : w_a) {
    ASSERT_LE((p->getNatural() - w_a_natural).array().abs().sum(), kEps);
  }

  McFloat elbo = learner.elbo(obs);
#ifdef MIMIC_DEBUG
  ASSERT_NEAR(elbo, -10.84904194f, kEps);
#else
  ASSERT_NEAR(elbo, -10.84903812f, kEps);
#endif
}

// Test constructor with everything set to default.
TEST(VBHMM, BatchSVIStepDefault) {
  McInt constexpr n_symbols = 4;
  McInt constexpr n_states = 2;
  McInt constexpr subchain_length = 4;
  McInt constexpr buffer_length = 2;

  Eigen::MatrixXi obsi (1, 10);
  obsi << 3, 0, 1, 2, 0, 0, 0, 1, 0, 0;

  McFloat constexpr learning_rate = 0.5;
  MatrixXfR obsf = obsi.cast<McFloat>();
  VBHMMMock mock(n_states, n_symbols,
                 learning_rate,
                 subchain_length, buffer_length, 1, false);

  mock.batchSVIStep(obsf);

  // w_A
  Eigen::VectorXf const& w_a_0 = mock.w_a_[0]->getNatural();
  Eigen::VectorXf const& w_a_1 = mock.w_a_[1]->getNatural();
  std::vector<McFloat> w_a_v { 3.16666667, 3.16666667 };
  for (auto i : Range(w_a_v.size())) {
    ASSERT_NEAR(w_a_0[i], w_a_v[i], kEps);
    ASSERT_NEAR(w_a_1[i], w_a_v[i], kEps);
  }

  // u_A
  Eigen::VectorXf const& u_a_0 = mock.emissions_[0]->prior()->getNatural();
  Eigen::VectorXf const& u_a_1 = mock.emissions_[1]->prior()->getNatural();;
  std::vector<McFloat> u_a_v { 6.68306637, 3.56102228, 3.56102224, 2.0 };
  for (auto i : Range(u_a_0.size())) {
    ASSERT_NEAR(u_a_0(i), u_a_v[i], kEps);
    ASSERT_NEAR(u_a_1(i), u_a_v[i], kEps);
  }


  McFloat elbo = mock.elbo(obsf);
  ASSERT_NEAR(elbo, -19.00878906, kEps);
}

TEST(VBHMM, SVIStepReproduce) {
  McInt constexpr n_rounds = 4;
  McInt constexpr n_iters = 16;

  std::vector<std::vector<McFloat>> elbos(n_rounds);
  Eigen::MatrixXi obsi = Eigen::MatrixXi::Random(2, 35);

  McInt constexpr n_symbols = 16;
  McInt constexpr n_states = 10;

  for (auto i : Range(obsi.size())) {
    if (obsi.data()[i] <= 0) {
      obsi.data()[i] = (- obsi.data()[i]) + 1;
    }
    obsi.data()[i] %= n_symbols;
  }

  MatrixXfR obsf = obsi.cast<McFloat>();

  McInt constexpr subchain_length = 10;
  McInt constexpr buffer_length = 5;
  McFloat constexpr learning_rate = 0.5;
  for (auto round : Range(0, n_rounds)) {
    VBHMMMock mock(n_states, n_symbols,
                   learning_rate, subchain_length, buffer_length, 2, false);
    for (auto iter : Range(0, n_iters)) {
      mock.batchSVIStep(obsf);
      McFloat elbo = mock.elbo(obsf);
      elbos.at(round).push_back(elbo);
    }
  }

  auto first_elbos = elbos[0];
  for (McInt i = 1; i < elbos.size(); ++i) {
    size_t s = elbos[i].size();
    for (auto j : Range(first_elbos.size())) {
      ASSERT_EQ(first_elbos[j], elbos[i][j]);
    }
  }
}

// --- Full step ---
TEST(VBHMM, FullVBStep) {
  McInt constexpr n_symbols = 4;
  McInt constexpr n_states = 2;

  Eigen::MatrixXi obsi (1, 10);
  obsi << 3, 0, 1, 2, 0, 0, 0, 1, 0, 0;

  VBHMMMock mock(n_states, n_symbols, 0.5);

  MatrixXfR obsf = obsi.cast<McFloat>();

  mock.fullVBStep(obsf);
  McFloat elbo = mock.elbo(obsf);

  // D
  auto emissions = mock.emissions_;
  std::vector<McFloat> emission_sol {
    12.7041525, 5.56805083, 3.78402542, 3.78402542};
  for (auto& p : emissions) {
    auto natural = p->prior()->getNatural();
    for (auto i : Range(emission_sol.size())) {
      ASSERT_NEAR(natural[i], emission_sol[i], kEps);
    }
  }

  // w_PI
  auto w_pi = mock.w_pi->getNatural();
  std::vector<McFloat> w_pi_v {2.5, 2.5};
  for (auto i : Range(w_pi_v.size())) {
    ASSERT_NEAR(w_pi[i], w_pi_v[i], kEps);
  }

#ifdef MIMIC_DEBUG
  ASSERT_NEAR(mock.elbo(obsf), -15.85983111f, kEps);
#else
  ASSERT_NEAR(mock.elbo(obsf), -15.859846115, kEps);
#endif
}

TEST(VBHMM, FullVBStepReproduce) {
  McInt constexpr n_rounds = 4;
  McInt constexpr n_iters = 16;

  std::vector<std::vector<McFloat>> elbos(n_rounds);
  Eigen::MatrixXi obsi = Eigen::MatrixXi::Random(1, 35);

  McInt constexpr n_symbols = 16;
  McInt constexpr n_states = 10;

  for (auto i : Range(obsi.size())) {
    if (obsi.data()[i] <= 0) {
      obsi.data()[i] = (- obsi.data()[i]) + 1;
    }
    obsi.data()[i] %= n_symbols;
  }

  MatrixXfR obsf = obsi.cast<McFloat>();
  for (auto round : Range(0, n_rounds)) {
    VBHMMMock mock(n_states, n_symbols, 0.5);
    for (auto iter : Range(0, n_iters)) {
      mock.fullVBStep(obsf);
      McFloat elbo = mock.elbo(obsf);
      elbos.at(round).push_back(elbo);
    }
  }

  auto first_elbos = elbos[0];
  for (McInt i = 1; i < elbos.size(); ++i) {
    size_t s = elbos[i].size();
    for (auto j : Range(first_elbos.size())) {
      ASSERT_EQ(first_elbos[j], elbos[i][j]);
    }
  }
}

TEST(VBHMM, IO) {
  McInt constexpr n_symbols = 16;
  McInt constexpr n_states = 10;

  nih::json::Json save_handle { nih::json::Object() };
  nih::json::Json load_handle { nih::json::Object() };

  Eigen::MatrixXi obsi = Eigen::MatrixXi::Random(1, 35);
  for (auto i : Range(obsi.size())) {
    if (obsi.data()[i] <= 0) {
      obsi.data()[i] = (- obsi.data()[i]) + 1;
    }
    obsi.data()[i] %= n_symbols;
  }
  MatrixXfR obsf = obsi.cast<McFloat>();

  {
    VBHMMMock mock(n_states, n_symbols, 0.5);
    mock.fullVBStep(obsf);
    mock.save(save_handle);
  }
  {
    VBHMMMock mock;
    mock.load(save_handle);
    mock.save(load_handle);
  }

  std::stringstream ss;
  std::stringstream ds;

  nih::json::Json::dump(save_handle, &ss);
  nih::json::Json::dump(load_handle, &ds);

  ASSERT_EQ(ss.str(), ds.str());
}

}  // namespace model
}  // namespace mimic