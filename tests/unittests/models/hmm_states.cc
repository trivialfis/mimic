#include <gtest/gtest.h>
#include <Eigen/Dense>

#include "mimic/models/hmm_states.hh"
#include "mimic/basic/eigen_types.hh"
#include "mimic/distributions/multinoulli.hh"
#include "mimic/basic/range.hh"

namespace mimic {
namespace model {

struct Params {
  McInt k_states;
  MatrixXfC transi;
  Eigen::VectorXf pi;
  std::vector<std::shared_ptr<dist::Distribution>> emits;
  MatrixXfR obs;
  MatrixXfC psi;
};

auto constructParams() {
  constexpr McInt k_states = 2;

  MatrixXfC transi(2, 2);
  transi(0, 0) = 0.9;
  transi(0, 1) = 0.1;
  transi(1, 0) = 0.9;
  transi(1, 1) = 0.1;

  Eigen::VectorXf probs[2] {Eigen::VectorXf(2), Eigen::VectorXf(2)};
  probs[0] << 0.9, 0.1;
  probs[1] << 0.2, 0.8;

  std::vector<std::shared_ptr<dist::Distribution>> emits (2);
  for (auto i : Range(2)) {
    emits.at(i).reset(new dist::Multinoulli(probs[i]));
  }

  Eigen::VectorXf pi (2);
  pi << 0.5, 0.5;

  MatrixXfC psi (5, 2);
  psi <<
      0.6, 0.4,
      0.3, 0.7,
      0.2, 0.8,
      0.7, 0.3,
      0.1, 0.9;

  MatrixXfR obs (1, 5);
  obs << 0, 0, 1, 0, 0;

  Params p;
  p.k_states = k_states;
  p.transi = transi;
  p.pi = pi;
  p.emits = emits;
  p.obs = obs;
  p.psi = psi;

  return p;
}

class DHmmMock : public DistHiddenMarkovModel {
  FRIEND_TEST(HMMState, Forward);
  FRIEND_TEST(HMMState, Backward);
  FRIEND_TEST(HMMState, EStepRowSubChain);
  FRIEND_TEST(HMMState, EStep);

 public:
  DHmmMock(McInt k_states, MatrixXfC transitions,
                Eigen::VectorXf pi,
                std::vector<std::shared_ptr<dist::Distribution>> const& emits,
                MatrixXfR obs) :
      DistHiddenMarkovModel(k_states, transitions,
                pi, emits, obs) {}
  DHmmMock() {

  }
  virtual ~DHmmMock() = default;
};

TEST(HMMState, Forward) {
  auto p = constructParams();
  DHmmMock state(p.k_states, p.transi, p.pi, p.emits, p.obs);

  auto const& res = state.forward(p.obs.row(0), p.psi);
  MatrixXfC alphas = res.first;
  Eigen::VectorXf z = res.second;

  MatrixXfC alphas_sol (2, 5);
  alphas_sol <<
      -0.598139, -0.153372,   -0.184368,   -0.0718369,  -0.220967,
      -0.798139, -1.95059645, -1.78159213, -2.66906166, -1.61819183;
  ASSERT_LE((alphas - alphas_sol).array().abs().sum(), kEps);

  Eigen::VectorXf z_sol (5);
  z_sol << 1.65697181, 1.416248321, 1.32181644, 1.94736325, 1.24061405;
  ASSERT_LE((z - z_sol).array().abs().sum(), kEps*10);
}

TEST(HMMState, Backward) {
  auto p = constructParams();
  DHmmMock state(p.k_states, p.transi, p.pi, p.emits, p.obs);

  MatrixXfC beta = state.backward(p.obs.row(0), p.psi);
  MatrixXfC beta_sol (p.psi.cols(), p.obs.size());
  beta_sol <<
      1.5091, 1.16109, 0.882083, 0.215606, 0,
      1.5091, 1.16109, 0.882083, 0.215606, 0;

  ASSERT_LE((beta_sol - beta).array().abs().sum(), kEps*25);
}

// eStep is ran during construction
TEST(HMMState, EStep) {
  auto p = constructParams();

  MatrixXfR obs (1, 5);
  obs << 0, 1, 1, 0, 1;
  DHmmMock state(p.k_states, p.transi, p.pi, p.emits, obs);

  auto transitions = state.transitions_;
  auto pi = state.pi_;

  MatrixXfC transitions_sol(2, 2);
  transitions_sol <<
      -0.10536052, -2.30258509,
      -0.10536052, -2.30258509;
  ASSERT_LE((transitions.array() - transitions_sol.array()).abs().sum(), kEps);
  Eigen::VectorXf pi_sol(2);
  pi_sol << -0.69314718, -0.69314718;
  ASSERT_LE((pi.array() - pi_sol.array()).abs().sum(), kEps);

  std::vector<McFloat> gammas_sol {
    -0.200671f,   -0.635989f, -0.635989f, -0.0243914f, -0.635989f,
    -1.70474809f, -0.753772f, -0.753772f, -3.72569343, -0.753772f};
  Eigen::Tensor<McFloat, 3> gammas = state.gammas_;
  for (auto i : Range(gammas.size())) {
    ASSERT_NEAR(gammas.data()[i], gammas_sol.at(i), kEps);
  }

  Eigen::Tensor<McFloat, 4> xi = state.xi_;
  /*
    [[[[-0.83665946 -0.9544425 ]
       [-2.34073686 -2.45851989]]

      [[-1.27197753 -1.38976057]
       [-1.38976057 -1.5075436 ]]

      [[-0.66038022 -4.36168219]
       [-0.77816326 -4.47946523]]

      [[-0.66038022 -0.77816326]
       [-4.36168219 -4.47946523]]]]
  */
  std::vector<McFloat> xi_sol {
    -0.83665946, -1.27197753,
    -0.66038022, -0.66038022,
    -2.34073686, -1.38976057,
    -0.77816326, -4.36168219,
    -0.9544425,  -1.38976057,
    -4.36168219, -0.77816326,
    -2.45851989, -1.5075436,
    -4.47946523, -4.47946523};
  for (auto i : Range(xi.size())) {
    ASSERT_NEAR(xi_sol.at(i), xi.data()[i], kEps);
  }
}

TEST(HMMState, IO) {
  nih::json::Json save_handle { nih::json::Object() };
  nih::json::Json load_handle { nih::json::Object() };

  {
    auto p = constructParams();
    DHmmMock state(p.k_states, p.transi, p.pi, p.emits, p.obs);
    state.save(save_handle);
  }
  {
    DHmmMock state;
    state.load(save_handle);
    state.save(load_handle);
  }

  std::stringstream ss;
  nih::json::Json::dump(save_handle, &ss);
  std::stringstream ds;
  nih::json::Json::dump(load_handle, &ds);
  ASSERT_EQ(ss.str(), ds.str());
}

}  // namespace model
}  // namespace mimic