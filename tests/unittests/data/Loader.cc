#include <gtest/gtest.h>

#include <string>
#include <fstream>
#include <memory>
#include <ext/stdio_filebuf.h>
#include <cstdio>
#include <cstdlib>

#include "mimic/data/Loader.hh"
#include "mimic/basic/FileSystem.hh"

namespace mimic {

TEST(Loader, Load) {
  TemporaryDirectory tmp_dir;
  auto dir_path = tmp_dir.GetPath();

  std::string filename = "/whatevs.txt";
  filename = dir_path + filename;

  std::ofstream fout(filename);
  fout << "0 1 2 3 4 5";
  fout << std::endl;
  fout.close();

  DataSource source {filename};
  DenseMatrixLoader loader {source};
  Matrix<McFloat>* matrix_ptr = loader.Load();
  std::unique_ptr<CsrMatrix<McFloat>> csrmatrix_ptr {
    reinterpret_cast<CsrMatrix<McFloat>*>(matrix_ptr) };

  CsrMatrix<McFloat> result {Dim(1, 6),
                              {0, 1, 2, 3, 4, 5},
                              {0},
                              {0, 1, 2, 3, 4, 5}};
  ASSERT_EQ(result, *csrmatrix_ptr);
}

}  // namespace mimic