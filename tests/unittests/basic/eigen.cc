#include <gtest/gtest.h>
#include <Eigen/Dense>

#include "mimic/basic/eigen_types.hh"

namespace mimic {

TEST(Eigen, Amax) {
  MatrixXfC value (4, 4);
  McFloat k = 0.0f;
  for (McInt r = 0; r < 4; ++r) {
    for (McInt c = 0; c < 4; ++c) {
      value(r, c) = k;
      k++;
    }
  }

  Eigen::VectorXf cmax = amax(value, 0);
  Eigen::VectorXf cmax_sol = value.row(3).transpose();
  ASSERT_EQ(cmax, cmax_sol);

  Eigen::VectorXf rmax = amax(value, 1);
  Eigen::VectorXf rmax_sol = value.col(3);
  ASSERT_EQ(rmax, rmax_sol);
}

}  // namespace mimic