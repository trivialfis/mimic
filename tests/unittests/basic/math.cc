#include <gtest/gtest.h>
#include <vector>

#include "mimic/basic.h"
#include "mimic/basic/math.hh"

namespace mimic {

TEST(Math, LogSumExp) {
  std::vector<McFloat> input {1.0, 2.0, 3.0, 4.0};
  McFloat res = math::logSumExp(input.cbegin(), input.cend());
  McFloat sol = 4.440189699;
  ASSERT_NEAR(res, sol, kEps);
}

TEST(Math, AlogSumExp) {
  MatrixXfC value (3, 3);
  McFloat k = 0.0f;
  for (auto r : Range(value.rows())) {
    for (auto c : Range(value.cols())) {
      value(r, c) = k;
      k++;
    }
  }

  {
    Eigen::VectorXf res = math::logSumExp(value, 0);
    Eigen::VectorXf sol(3);
    sol << 6.05094576, 7.05094576, 8.05094576;
    ASSERT_LE((res.array() - sol.array()).abs().sum(), kEps);
  }

  {
    Eigen::VectorXf res = math::logSumExp(value, 1);
    Eigen::VectorXf sol(3);
    sol << 2.40760596, 5.40760596, 8.40760596;
    ASSERT_LE((res.array() - sol.array()).abs().sum(), kEps);
  }
}

}  // namespace mimic