#include <gtest/gtest.h>

#include "mimic/basic/random.hh"
#include "mimic/basic/range.hh"

namespace mimic {

TEST(Random, Device) {
  RandomDevice::seed(1);
  int seed = RandomDevice::gen();
  ASSERT_EQ(seed, 1);

  RandomDevice::reset();
  seed = 0;
  for (auto i : Range(0, 16)) {
    seed += RandomDevice::gen();
  }
  ASSERT_NE(seed, 16);
}

TEST(Random, Context) {
  RandomContext context;
  {
    RandomContext c(10);
    context = std::move(c);
    ASSERT_FALSE(c.shouldReset());
  }
  ASSERT_TRUE(context.shouldReset());

  ASSERT_EQ(RandomDevice::gen(), 10);
}

}  // namespace mimic