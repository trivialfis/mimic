#include <mimic/basic.h>
#include <gtest/gtest.h>

#include <string>

#include "mimic/basic/strings.hh"

template<typename T>
struct IsDouble {
  static bool constexpr value = false;
};
template <>
struct IsDouble<double> {
  static bool constexpr value = true;
};

template <typename T>
struct IsFloat {
  static bool constexpr value = false;
};
template <>
struct IsFloat<float> {
  static bool constexpr value = true;
};

template <typename T>
struct IsInt {
  static bool constexpr value = false;
};
template <>
struct IsInt<int> {
  static bool constexpr value = true;
};

namespace mimic {

TEST(Strings, StoN) {
  auto f = ston<float>("1.1");
  static_assert(IsFloat<decltype(f)>::value, "Return type must be float.");
  ASSERT_NEAR(f, 1.1, kEps);

  auto d = ston<double>("1.1");
  static_assert(IsDouble<decltype(d)>::value, "Return type must be double.");
  ASSERT_NEAR(d, 1.1, kEps);

  auto i = ston<int>("1");
  static_assert(IsInt<decltype(i)>::value, "Return type must be int.");
  ASSERT_EQ(i, 1);
}

TEST(Strings, Split) {
  std::string input {"Hello, world"};
  std::vector<std::string> result;
  Split(result, input, [](char c){ return c == ','; });
  ASSERT_EQ(result.size(), 2);
  ASSERT_EQ(result.at(0), "Hello");
  ASSERT_EQ(result.at(1), " world");

  input = "Hello";
  Split(result, input, [](char c){ return c == ','; });
  ASSERT_EQ(result.at(2), "Hello");

  input = "";
  Split(result, input, [](char c){ return c == ','; });
  ASSERT_EQ(result.size(), 3);
}

}  // namespace mimic
