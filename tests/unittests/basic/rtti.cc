//===---------- llvm/unittest/Support/Casting.cpp - Casting tests ---------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT included in llvm repository for details.
//
//===----------------------------------------------------------------------===//

#include <gtest/gtest.h>

#include "mimic/basic/rtti.hh"

namespace mimic {

struct Base {
  bool is_derived_;
  Base(bool is_derived = false) : is_derived_{is_derived} {}
};

struct Derived : Base {
  Derived() : Base(true) {}
  static bool isClassOf(const Base *B) { return B->is_derived_; }
};

struct NotDerived {
  static bool isClassOf(const Base* B) { return false; }
};

TEST(RTTI, IsA) {
  Base B;
  Derived D;
  ASSERT_TRUE(isa<Base>(D));

  Base* pb = new Derived;
  ASSERT_TRUE(isa<Derived>(pb));
  ASSERT_TRUE(isa<Base>(pb));
  delete pb;
}

TEST(RTTI, Cast) {
  Base B;
  Derived D;
  Base* pb = new Derived;

  auto casted = cast<Derived>(pb);
  static_assert(std::is_same<Derived*, decltype(casted)>::value,
                "type deduction failed.");
  delete pb;
}

}  // namespace mimic