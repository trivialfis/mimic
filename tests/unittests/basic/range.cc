#include <gtest/gtest.h>

#include "mimic/basic/range.hh"

namespace mimic {

TEST(Basic, Range) {
  McInt j = 0;
  for (auto i : Range(0, 10)) {
    ASSERT_EQ(i, j);
    j++;
  }
  ASSERT_EQ(j, 10);

  j = 0;
  for (auto i : Range(10)) {
    ASSERT_EQ(i, j);
    j++;
  }
  ASSERT_EQ(j, 10);

  j = 0;
  for (auto i : Range(0, 10, 2)) {
    ASSERT_EQ(i, j);
    j += 2;
  }
  ASSERT_EQ(j, 10);
}

}  // namespace mimic