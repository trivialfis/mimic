#include <memory>
#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <mimic/basic.h>

#include "mimic/distributions/distribution.hh"
#include "mimic/distributions/multinoulli.hh"
#include "mimic/distributions/dirichlet.hh"
#include "mimic/basic/range.hh"
#include "mimic/basic/eigen_types.hh"

#include "mimic/models/vbhmm.hh"

mimic::McInt constexpr k_states = 2;
// WIP
namespace mimic {

// model::HMM makeHMM() {
//   McFloat constexpr t_0 = 0.9;
//   McFloat constexpr t_1 = 0.1;
//   std::vector<std::shared_ptr<dist::Distribution>> d;
//   for (auto& p : d) {
//     p.reset(new dist::Multinoulli(Eigen::VectorXf::Constant(k_states, 0.5)));
//   }
//   return model::HMM(k_states,
//                     MatrixXfC::Constant(k_states, k_states, 0.5),
//                     Eigen::VectorXf::Constant(k_states, 0.5),
//                     d);
// }

model::VBHMM makeVBHMM() {
  std::shared_ptr<dist::Dirichlet> u_a {
    new dist::Dirichlet(Eigen::VectorXf::Constant(2, 3.0f))
  };
  std::shared_ptr<dist::Dirichlet> u_pi {
    new dist::Dirichlet(Eigen::VectorXf::Constant(2, 2.0f))
  };

  std::vector<std::shared_ptr<dist::Dirichlet>> u_d (2);
  for (auto& p : u_d) {
    p.reset(new dist::Dirichlet(Eigen::VectorXf::Constant(2, 3.0f)));
  }

  std::vector<std::shared_ptr<dist::Exponential>> d (2);
  for (auto& p : d) {
    p.reset(new dist::Multinoulli(Eigen::VectorXf::Constant(2, 0.5f)));
  }

  return model::VBHMM(k_states, u_a, u_pi, u_d, d);
}

void runSVI(McInt buf_size, McInt subchain_len, McFloat rate,
            MatrixXfR obs) {
  McInt constexpr n_iters = 10;
  std::vector<McFloat> elbos(n_iters);
  model::VBHMM learner { makeVBHMM() };
  // for (auto iter : mimic::Range(0, n_iters)) {
  //   learner.batchSVIStep(obs);
  // }
}

TEST(VBHMM, SVIStep) {
  McInt constexpr buf_size = 2;
  McInt constexpr subchain_len = 10;
  McFloat constexpr rate = 0.5;
  MatrixXfR obs = MatrixXfR::Random(2, 100);
  // runSVI(buf_size, subchain_len, rate, obs);
}

}  // namespace mimic